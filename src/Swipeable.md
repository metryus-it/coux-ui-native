# Swipeable
Swipeable component allows for implementing swipeable rows or similar interaction. It renders its children within a panable container allows for horizontal swiping left and right.

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| RTL | boolean | justifies whether the component should include RTL functionality. |
| children | node | Utility for opaque data structure. |
| customElementEnd | component | Element that can replace right section. ( <Text>example</Text> ). |
| customElementStart | component | Element that can replace left section.( <Text>example</Text> ). |
| size | number | size of an icon. |
| activeOpacity | number | active opacity on option press (if not replaced by the component). |
| labelType | string | Type label for component text. |
| buttonsStart | array | Array config buttons for left side. |
| buttonsEnd | array | Array config buttons for right side. |
