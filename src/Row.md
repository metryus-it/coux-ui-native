# Row
Row with display flex and flex-direction row, either the normal row or with the RTL functionallity, all the alignments may   be controlled by props;

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| RTL | bool | justifies whether the component should include the RLT functionality. |
| align | string | usual flex align-items props ( aka flex-start, flex-end ...). |
| justify | string | usual flex justify-content props ( aka flex-start, flex-end ...). |
| horizontalPadding | number | Horizontal padding value. Default value comes from theme (theme.metrics.baseHirozontalPadding) |
| noFlex | bool | Disable all flex and RTL functionality. You receive simple View with base margins |
