/**
 * Component that expands standart TextInput component with extra icons, texts and custom styling. Receive all native TextInput's props and extra props descibed below
 * @name TextInput
 * @param {string} [iconStart] - Icon name (see Icon component) that will appear at left side of input (in RTL mode)
 * @param {string} [iconStartSize] - Left icon size
 * @param {function} [iconStartAction] - Left icon's action
 * @param {string} [iconStart] - Icon name (see Icon component) that will appear at right side of input (in RTL mode)
 * @param {string} [iconStartSize] - Right icon size
 * @param {function} [iconStartAction] - Right icon's action
 * @param {string} [label] - Label that will appear above input
 * @param {string} [topText] - Text above input on right side. May be clickable
 * @param {function} [topTextAction] - Top text action
 * @param {boolean} [RTL = false] - justifies whether the component should include the RLT functionality.
 * @param {boolean} [error = false] - When true highlights hint message and input's border with red color
 * @param {boolean} [disabled] - Use textInput as readonly
 * @param {boolean} [noBorder] - Make transparent border color
 * @param {boolean} [noBg] - Make transparent background color
 * @param {boolean} [isUppercase] - Retrun uppercased string
 * @param {string} [hint] - Text below input
 * @param {function} [onPress] - If you want to use TextInput component as button, use onPress prop. Other actions such as onTextChange, topTextAction will be ignored.
 * @param {object} [inputStyle] - Preset (initial set) input custom style. Some styles will be rewriten by props conditions.
 * @param {function} [infoIconAction] - Add this function and small clickable 'info' icon will appear next to label.
 * @returns {component}
 */

import React, { Component } from 'react';
import { TouchableOpacity, Platform } from 'react-native';
import styled, { withTheme } from 'styled-components/native';
import { getValue } from '../defaults';
import Icon from './Icon';
import View from './View';
import Text from './Text';
import TextInputMask from 'react-native-text-input-mask';
import R from 'ramda';

// const Wrapper = styled(TouchableOpacity)(getValue('textInput.wrapper'));
const Wrapper = styled(View)(getValue('textInput.wrapper'));
const InputHolder = styled(View)(props => {
  const { noBorder, disabled, error } = props;
  let borderColorKey = 'textInput.borderColor';
  if (noBorder) borderColorKey = 'colors.transparent';
  if (disabled) borderColorKey = 'colors.grayishBlue';
  if (error) borderColorKey = 'textInput.borderColorError';

  return ({
    ...getValue('textInput.inputHolder')(props),
    borderColor: getValue(borderColorKey)(props)
  })
})
const StyledTextInput = styled.TextInput(getValue('textInput.input'));
const StyledTextInputMask = styled(TextInputMask)(getValue('textInput.input'));
const ViewForText = styled(View)(getValue('textInput.inputView'));
const TextOnly = styled(Text)(getValue('textInput.inputText'));
const TopRow = styled(View)(getValue('textInput.topRow'));
const TextRegular = styled(Text)(props => {
  const fontWeight = getValue('textInput.labelFontWeight')(props) || 'normal';
  const color = getValue('textInput.labelColor')(props);
  const colorFocus = getValue('textInput.labelColorFocus')(props);
  return ({
    fontSize: getValue('textInput.labelFontSize')(props),
    color: props.isActive && colorFocus ? colorFocus : color,
    fontWeight: fontWeight
	});
});
const TextLink = styled(Text)`
  color: ${getValue('textInput.linkColor')}
`;
const IconHolder = styled(TouchableOpacity)`
align-self: stretch;
justify-content: center;
paddingHorizontal: ${getValue('textInput.paddingHorizontal')};
`;
const Hint = styled(Text)`
  color: ${props => getValue('colors.' + (props.isError ? 'alert' : 'main'))};
  fontSize: ${props => getValue('textInput.hintFontSize')};
  margin-top: ${props => !R.isNil(props.marginTop) ? props.marginTop : getValue('textInput.wrapper.marginTop')};
  marginHorizontal: ${ props => !R.isNil(props.marginHorizontal) ? props.marginHorizontal : getValue('textInput.wrapper.marginHorizontal')}
`;

class TextInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: false,
      text: '',
      hasValue: (this.props.value || this.props.defaultValue),
      showHint: false,
    };

    this._onChangeText = this._onChangeText.bind(this);
    this.renderIcon = this.renderIcon.bind(this);
  }

  _onChangeText = (text, textUnmasked) => {
    let val = text;
    if (this.props.isUppercase && typeof text === 'string') {
      val = text.toUpperCase();
    }
    if (this.props.mask && this.props.returnUnmasked) {
      val = textUnmasked;
    }
    val = this.prepareValue(val);
    this.setState({
      text: text
    });
    this._callback('onChangeText', val);
    this._callback('onTextChange', val);
  };


  _callback(name, e) {
    if (this.props[name]) {
      this.props[name](e);
    }
  }

  prepareValue(val) {
    const { keyboardType } = this.props;
    if (typeof val === 'undefined') {
      return val;
    }
    let value = val;
    switch (keyboardType) {
      case 'numeric':
      case 'amount':
        value = value.replace(/([^0-9\.])*/g, '');
        value = value.replace(/^(\.)*/, '');
        const dotIndex = value.indexOf('.');
        if (value && dotIndex !== -1) {
          const beforeDot = value.substr(0, dotIndex + 1);
          const afterDot = value.substr(dotIndex + 1).replace(/\D/g, '').substr(0, 2);
          value = beforeDot + afterDot;
        }
        break;
      case 'digit':
      case 'digits':
        value = value.replace(/([^\D])*/g, '');
        break;
      default:

    }
    return value.toString();
  }

  _onFocus = (e) => {
    this.setState({ inFocus: true });
    if (this.props.hint) {
      this.setState({ showHint: true });
    }
    this._callback('onFocus', e);
  };

  _onBlur = (e) => {
    this.setState({ inFocus: false });
    if (this.props.hint) {
      this.setState({ showHint: false });
    }
    this._callback('onBlur', e);
  };

  renderIcon(iconName, iconSize, action) {
    const iconClickable = action && typeof action === 'function';
    if (iconName) {
      return (
        <IconHolder
          onPress={iconClickable ? action : () => { }}
          activeOpacity={iconClickable ? .7 : 1}
        >
          <Icon name={iconName} size={iconSize || getValue('textInput.wrapper.iconSize')(this.props)} />
        </IconHolder>
      );
    } else {
      return null;
    }
  }

  render() {
    const { error, mask, iconStart, iconStartAction, iconStartSize, iconEnd, infoIconAction,
      iconEndAction, iconEndSize, disabled, noBorder, onPress, noBg, RTL, inputStyle, styleViewForText } = this.props;

    const grayishBlue = getValue('colors.grayishBlue')(this.props)
    const disabledBg = getValue('colors.disabledBg')(this.props)

    let textInputStyle = {
			...(inputStyle || {}),
      paddingHorizontal: iconStart ? getValue('textInput.paddingHorizontal')(this.props) : 0,
      backgroundColor: getValue(`colors.${noBg ? 'transparent' : 'white'}`)(this.props),
      color: getValue('textInput.color')(this.props)
    }

    if (disabled) {
      textInputStyle.color = grayishBlue;
      textInputStyle.backgroundColor = disabledBg;
    }
    // if (bigText) {
    //   textInputStyle.fontSize = 28;
    //   if (Platform.OS === 'android') {
    //     textInputStyle.paddingTop = 10;
    //     textInputStyle.paddingBottom = 5;
    //   }
    // }

    const specialProps = {
      mask: mask
    };

    const noAction = () => null;
    const hasOnPress = onPress && typeof onPress === 'function';

    let TextInputComponent = StyledTextInput;
    if (mask) {
      TextInputComponent = StyledTextInputMask;
    }

    let autoCorrect = typeof this.props.autoCorrect === 'undefined' ? true : this.props.autoCorrect;
    if (this.props.secureTextEntry) {
      autoCorrect = false;
    } else {
      specialProps.autoCorrect = autoCorrect;
    }

    let label;
    if (this.props.label) {
      label = (
        <TextRegular RTL={RTL} isActive={!!this.state.text}>
          {this.props.label} {infoIconAction ? 
            <TouchableOpacity onPress={infoIconAction}>
              <Icon name="info" size={16} />
            </TouchableOpacity>
          : null}
        </TextRegular>
      );
    }


    let topText;
    const topTextClickable = this.props.topTextAction && typeof this.props.topTextAction === 'function';

    if (this.props.topText) {
      topText = (
        <TouchableOpacity
          onPress={topTextClickable ? this.props.topTextAction : () => { }}
          activeOpacity={topTextClickable ? .7 : 1}
        >
          {topTextClickable ?
            <TextLink RTL={RTL}>{this.props.topText}</TextLink> :
            <TextRegular RTL={RTL}>{this.props.topText}</TextRegular>
          }
        </TouchableOpacity>
      );
    }

    let hint;
    if (this.props.hint) {
      hint = (
        <Hint isError={error} RTL={RTL}>{this.props.hint}</Hint>
      );
    }

    return (
      <Wrapper
      // onPress={ hasOnPress ? onPress : noAction }
      // activeOpacity={ hasOnPress ? .8 : 1}
      >
        {label || topText ? <TopRow row justify='space-between'>
          {label}
          {topText}
        </TopRow> : null}
        <TouchableOpacity
          onPress={hasOnPress ? onPress : noAction}
          activeOpacity={hasOnPress ? .8 : 1}
        >
          <InputHolder
            row
            align='center'
            disabled={disabled}
            noBorder={noBorder}
            error={error}
            RTL={RTL}
          >
            {this.renderIcon(iconStart, iconStartSize, iconStartAction)}
            {hasOnPress ?
              <ViewForText style={styleViewForText}>
                <TextOnly>{this.props.value}</TextOnly>
              </ViewForText>
              :
              <TextInputComponent
                {...this.props}
                onChangeText={this._onChangeText.bind(this)}
                onFocus={this._onFocus}
                onBlur={this._onBlur}
                editable={disabled ? false : this.props.editable}
                style={textInputStyle}
                placeholderTextColor={getValue('textInput.placeholderColor')(this.props)}
                {...specialProps}
              />
            }
            {this.renderIcon(iconEnd, iconEndSize, iconEndAction)}
          </InputHolder>
        </TouchableOpacity>
        {hint}
      </Wrapper>
    )
  }
}

export default withTheme(TextInput);
