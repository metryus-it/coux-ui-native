# PickerSelect
PickerSelect the form element that emulates a web select component.

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| iconStart | string | Icon name (see Icon component) that will appear at left side of item (in RTL mode). |
| iconEnd | string | Icon name (see Icon component) that will appear at right side of item (in RTL mode). |
| size | number | Icon width and height. |
| text | string | Main text that will appear at left side of component or right (in RTL mode). |
| textType | string | Main text styling (See Text component). Possible options - ``h1``, ``h2``, ``sm``, ``lg``, etc. |
| borderSize | number | Bottom border width of item, if not needed use 0. Default border height and color discribed at defaultTheme. |
| longText | bool | Make main text column wider in propotion of 2/3 with secondary one. Use this to put long text in primary text column. |
| RTL | bool | justifies whether the component should include the RLT functionality. |
| onPress | function | Function that will be called on item pressing. |
| hint | string | Text below input. |
| label | string | Text that will appear above input. |
| error | string | When true highlights hint message and input's border with red color. |
| onChange | function | Return changed value as a callback |
