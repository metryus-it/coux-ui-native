/**
 * A component designed to add vertical indentation between elements, as well as display a horizontal line.
 * @name Button
 * @param {number} [height] - Divider height
 * @param {string} [size] - Predefined width of divider. 'base', 'm', 'tiny', 'xs', 'small', 's', 'large', 'l', 'x-large', 'xl'
 * @param {bool} [bordered] - Add 1px bottom border to divider
 * @param {string} [borderColor] - Color of border 'colors.main' by default.
 * @param {number} [width] - Divider width. Makes sense if border enabled only
 * @param {number} [opacity] - Opacity of divider's border
 * @param {object} [style] - Divider custom style
 * @returns {component}
 */

import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import styled from 'styled-components/native';
import { getValue } from '../defaults';

const helperDivider = (size) => {
  let divSize;
  switch (size) {
    case '':
    case 'base':
    case 'm':
    case false:
    case undefined:
      divSize = 24;
      break;
    case 'tiny':
    case 'xs':
      divSize = 8;
      break;
    case 'small':
    case 's':
      divSize = 16;
      break;
    case 'large':
    case 'l':
      divSize = 48;
      break;
    case 'x-large':
    case 'xl':
      divSize = 64;
      break;
    default:
      divSize = parseInt(size);
  }
  return divSize
}

const StyledViewDivider = styled.View`
	border-bottom-color: ${props => props.borderColor ? getValue(props.borderColor)(props) : getValue('colors.main')(props)};
	width: ${ props => props.width ? props.width : props.bordered ? '100%' : helperDivider(props.size)};
	border-bottom-width: ${ props => props.bordered ? 1 : 0};
	height: ${ props => helperDivider(props.size)};
	opacity: ${ props => props.opacity ? props.opacity : 1};
`;

export default class Divider extends Component {

  render() {
    const { style } = this.props;

    return (
      <StyledViewDivider {...this.props} style={style} />
    );
  }
}
