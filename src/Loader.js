/**
 * Loader displays a circular loading indicator.
 * @name Loader
 * @returns {component}
 */

import React from 'react';
import { ActivityIndicator } from 'react-native';
import View from './View';

const Loader = (props) => {
	return (
		<View>
			<ActivityIndicator
				{...props}
			/>
		</View>
	);
};

export default Loader;