# Switch
Switch is a component for getting/showing boolean value or to select from one out of two.

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| label | label | The label is the text shown  of the Switch. |
| onChange | onChange | Action callback, will return new Switch state |
| textComponent | textComponent | Part of the item that can replace label, comes from out, example ( <Text>example</Text> ). |
| textType | textType | Comes from out indicates type ( ``h1``, ``h2``, ``h3`` ...). |
| errorText | errorText | Takes a parameter from to errorText for view message. |
| errorTextType | errorTextType | Parameter that indicates the type of text. |
| numberOfLines | numberOfLines | Takes a parameter from to label the number of lines ( 3,4,5...). |
| disabled | disabled | Blocks access and change of the form field ( true - false ). |
| disabledTextType | disabledTextType | Looks at disabled and if true add defualt style disabled for text. |
| textHorizPadding | textHorizPadding | Horizontal padding for text. |
