# ListItem
Component purposed to display one list item, with one or two column text and left/icons or custom components. Optionally clickable

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| componentStart | component | Custom component displayed on left side of item (in RTL mode) |
| componentEnd | component | Custom component displayed on right side of item (in RTL mode) |
| iconStart | string | Icon name (see Icon component) that will appear at left side of item (in RTL mode) |
| iconEnd | string | Icon name (see Icon component) that will appear at right side of item (in RTL mode) |
| text | string | Main text that will appear at left side of component or on center part if textEnd is absent (in RTL mode) |
| textEnd | string | Secondary text that will appear at right side of component (in RTL mode). Optional |
| textType | string | Main text styling (See Text component). Possible options - ``h1``, ``h2``, ``sm``, ``lg`` etc |
| textEndType | string | Secondary text styling (See Text component). Possible options - ``h1``, ``h2``, ``sm``, ``lg`` etc |
| borderSize | number | Bottom border width of item, if not needed use 0. Default border height and color discribed at defaultTheme |
| longText | boolean | Make main text column wider in propotion of 2/3 with secondary one. Use this to put long text in primary text column |
| longTextEnd | boolean | Make end text column wider in propotion of 2/3 with secondary one. |
| centeredText | boolean | Makes primary text column align center |
| RTL | boolean | justifies whether the component should include the RLT functionality. |
| onPress | function | Function that will be called on item pressing |
