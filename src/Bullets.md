# Bullets
Bullets indicator for carousels, sliders etc.

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| style | object | Wrapper's style |
| number | string | Number of bullets |
| current | string | Index of active bullet |
