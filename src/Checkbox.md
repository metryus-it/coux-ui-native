# Checkbox
Checkbox use control parameter with two states - ☑ checked and ☐ !checked && disabled;

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| label | string | Checkbox label. |
| checked | bool | Shows the state of the checkbox [true, false]. |
| onChange | function | Function to execute on change. |
| textComponent | component | Part of the item that can replace label, example [ <Text>example</Text> ]. |
| textType | string | Comes from out indicates type [``h1``, ``h2``, ``h3``, ``h4``...]. |
| iconActive | string | Takes an active icon name. |
| icon | string | Takes an icon name. |
| iconComponent | component | A component to replace Icon. |
| iconActiveComponent | component | A component to replace Active Icon. |
| size | number | Icon size as per the theme. |
| errorText | string | if there is an error - the text it should display, aka 'required'. |
| errorTextType | string | Parameter that indicates the type of text. |
| numberOfLines | number | Takes a parameter from to label the number of lines [2, 3, 4, 5...]. |
| disabled | bool | The dispabled state [true, false]. |
| RTL | bool | If should enable the RTL functionality [true, false]. |
| checkboxTextDisabled | bool | Looks at disabled and if true add default style disabled for text. |
