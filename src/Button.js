/**
 * Button component that able to change view, size, to add icons on the right and left side, to deactivate, to show loading status etc
 * @name Button
 * @param {string} [label] - Text that appears inside button
 * @param {string} [textType] - Label text type such as 'h1', 'h2', 'lg', 'sm' etc. See Text component readme for full list
 * @param {string} [type] - Determines button color that described at theme config. Could be as gradient or plain color (Possible options: 'Primary', 'Secondary', 'Success', 'Warning', 'Danger')
 * @param {string} [size] - Determines button's size. Takes such options as 'lg', 'df', 'sm', 'xs'. If not specified will use button default size (equally to 'df' option)
 * @param {string} [iconStart] - Icon name (see Icon component) that will appear at left button's side (or right side if RTL enabled)
 * @param {string} [iconEnd] - Icon name (see Icon component) that will appear at right button's side (or left side if RTL enabled)
 * @param {boolean} [link = false] - Switch button to transparent and no borders view
 * @param {boolean} [hollow = false] - Turn off button's background color and show border instead
 * @param {boolean} [disabled = false] - Makes button not clickable and 30% transparent
 * @param {boolean} [loading = false] - Shows spinner instead of text
 * @param {boolean} [RTL = false] - justifies whether the component should include the RLT functionality.
 * @param {boolean} [straightColor = false] - Setting button's label color as defined in theme button's 'type' section ('color'). Otherwise label color is white by default (except of 'hollow' or 'link' button's options)
 * @param {function} [onPress] - Function that will be called on button pressing
 * @returns {component}
 */

import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styled, { withTheme } from 'styled-components/native';
import { getValue } from '../defaults';
import Icon from './Icon';
import View from './View';
import Text from './Text';
import Loader from './Loader';

const BtnView = styled(View)`
border-width: ${props => props.hollow ? 2 : 0};
border-color: ${props => props.color};
opacity: ${props => props.disabled && !props.gradient ? .3 : 1};
`;

const LoaderHolder = styled.View`
position: absolute;
`;

class Button extends Component {
  render() {
    const { label, type, size = 'df', iconStart, iconEnd, link, hollow,
      disabled, loading, RTL, onPress, textType, straightColor } = this.props;
    const config = getValue(`button.type.${type}`)(this.props) || getValue('button.type.primary')(this.props);
    const sizeConfig = getValue(`button.size.${size}`)(this.props) || getValue('button.size.df')(this.props);
    const textColor = hollow || link || straightColor ? config.color : getValue('colors.white')(this.props);
    const backgroundColor = hollow || link || config.gradient ? 'transparent' : config.color;

    let content;
    const clickable = !(disabled || !onPress || typeof onPress !== 'function');
    const btnViewProps = {
      ...this.props,
      ...sizeConfig,
      ...config,
      textColor,
      backgroundColor
    }

    const iconStyleHelper = type => {
      const dir = getValue('dir', 'row')(this.props)
      let key;
      if (type === 'start') {
        key = RTL && dir === 'row-reverse' ? 'marginLeft' : 'marginRight';
      } else {
        key = 'marginLeft';
      }
      return {
        [key]: sizeConfig.paddingHorizontal / 2,
        opacity: loading ? 0 : 1,
      }
    }

    const btnView = (
      <BtnView {...btnViewProps} RTL={RTL} row align='center' justify='center'>
        {iconStart && <Icon name={iconStart} size={sizeConfig.iconSize} style={iconStyleHelper('start')} />}
        {!!loading &&
          <LoaderHolder><Loader size='small' color={textColor} /></LoaderHolder>
        }
        <Text
          type={textType}
          style={{ opacity: loading ? 0 : 1, color: textColor, fontSize: sizeConfig.fontSize || 20 }}
        >
          {label}
        </Text>
        {iconEnd && <Icon name={iconEnd} size={sizeConfig.iconSize} style={iconStyleHelper('end')} />}
      </BtnView>
    );

    if (config.gradient && !hollow && !link) {
      content = (
        <LinearGradient {...config.gradient} style={{ borderRadius: config.borderRadius, opacity: disabled ? .3 : 1 }}>
          {btnView}
        </LinearGradient>
      )
    } else {
      content = btnView;
    }

    return (
      <TouchableOpacity
        disabled={!clickable}
        activeOpacity={clickable ? .7 : 1}
        onPress={onPress}>
        {content}
      </TouchableOpacity>
    )
  }
}

export default withTheme(Button);
