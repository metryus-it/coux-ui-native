/**
 * Checkbox use control parameter with two states - ☑ checked and ☐ !checked && disabled;
 * @name Checkbox
 * @param {string} [label] - Checkbox label.
 * @param {bool} [checked] - Shows the state of the checkbox [true, false].
 * @param {function} [onChange] - Function to execute on change.
 * @param {component} [textComponent] - Part of the item that can replace label,
 *  example [ <Text>example</Text> ].
 * @param {string} [textType] - Comes from out indicates type [h1, h2, h3, h4...].
 * @param {string} [iconActive] - Takes an active icon name.
 * @param {string} [icon] - Takes an icon name.
 * @param {component} [iconComponent] - A component to replace Icon.
 * @param {component} [iconActiveComponent] - A component to replace Active Icon.
 * @param {number} [size] - Icon size as per the theme.
 * @param {string} [errorText] - if there is an error - the text it should display, aka 'required'.
 * @param {string} [errorTextType] - Parameter that indicates the type of text.
 * @param {number} [numberOfLines] - Takes a parameter from to label the number of lines [2, 3, 4, 5...].
 * @param {bool} [disabled] - The dispabled state [true, false].
 * @param {bool} [RTL] - If should enable the RTL functionality [true, false].
 * @param {bool} [checkboxTextDisabled] - Looks at disabled and if true add default
 *  style disabled for text.
 * @returns {component}
 */

import React from 'react';
import { TouchableOpacity } from 'react-native';
import View from './View';
import Icon from './Icon';
import Text from './Text';


const Checkbox = ({
	label,
	checked,
	onChange,
	textComponent,
	textType,
	iconActive,
	icon,
	iconComponent,
	iconActiveComponent,
	size,
	errorText,
	errorTextType,
	numberOfLines,
	disabled,
	disabledTextType,
	RTL,
	...props
}) => {

	return (
		<View>
			<TouchableOpacity
				disabled={!!disabled && disabled}
				accessibilityComponentType={
					!!checked ? "radiobutton_checked" : "radiobutton_unchecked"
				}
				accessibilityTraits={
					!!checked ? ["button", "selected"] : ["button"]
				}
				onPress={(onChange && typeof onChange === 'function') ? () => {
					onChange(!checked);
				} : undefined}
			>
				<View
					RTL={RTL}
					row
				>
					{!!checked && !!iconActiveComponent && iconActiveComponent}
					{!checked && !!iconComponent && iconComponent}
					{!!checked && !!iconActive && <Icon size={size} name={iconActive} />}
					{!checked && !!icon && <Icon size={size} name={icon} />}
					{!iconActiveComponent && !iconComponent && !iconActive && !icon && <Icon size={size} name={!checked ? 'checkbox' : 'checkboxActive'} />}
					<Text
						numberOfLines={numberOfLines}
						{...props}
						type={!disabled ? textType : disabledTextType}
					>
						{label || textComponent}
					</Text>
				</View>

			</TouchableOpacity>
			{
				!!errorText &&
				<View
					RTL={RTL}
				>
					<Text type={errorTextType}>
						{errorText}
					</Text>
				</View>
			}
		</View>
	);
};


export default Checkbox;
