/**
 * View with display flex, either the normal view or with the RTL functionallity,
 * all the flex alignments may be controlled by props
 * @name View
 * @param {boolean} [noRTL = false] - justifies whether the component should include
 *  the RLT functionality.
 * @param {boolean} [row = false] - Makes flex-direction to be row if true.
 * @param {string} [align] - usual flex align-items props ( aka flex-start, flex-end ...).
 * @param {string} [justify] - usual flex justify-content props ( aka flex-start, flex-end ...).
 * @returns {component};
 */

import React from 'react';
import styled from 'styled-components/native';
import { getValue, parseClassName } from '../defaults';

const ViewNormal = styled.View(props => {
  const classNameStylesArray = parseClassName(props);
  const defaultStyles = {
    display: 'flex',
    flexDirection: props.row ? 'row' : 'column',
    alignItems: props.row ? (props.align ? props.align : 'center') : (props.align ? props.align : 'stretch'),
    justifyContent: props.justify ? props.justify : 'flex-start',
  }
  return Object.assign({}, ...classNameStylesArray, defaultStyles);
});

const ViewRTL = styled(ViewNormal)(props => {
  const classNameStylesArray = parseClassName(props);
  let align;
  const hasColAndAlign = !props.row && !!props.align;

  if (props.row) {
    align = props.align ? props.align : 'center';

  } else if (hasColAndAlign && props.align !== 'flex-start' && props.align !== 'flex-end') {
    align = props.align;

  } else if (hasColAndAlign && props.align === 'flex-start') {
    align = getValue('dir', 'start')(props);

  } else if (hasColAndAlign && props.align === 'flex-end') {
    align = getValue('dir', 'end')(props);

  } else {
    align = 'stretch';
  }

  const defaultStyles = {
    flexDirection: props.row ? getValue('dir', 'row')(props) : 'column',
    alignItems: align,
  }
  return Object.assign({}, ...classNameStylesArray, defaultStyles);
});


const AppView = ({ children = false, noRTL, ...props }) => {
  if (noRTL) {
    return (
      <ViewNormal {...props}>
        {children}
      </ViewNormal>

    );
  } else {
    return (
      <ViewRTL {...props}>
        {children}
      </ViewRTL>
    );
  }
};

export default AppView;
