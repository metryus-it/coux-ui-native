/**
 * Swipeable component allows for implementing swipeable rows or similar interaction.
 * It renders its children within a panable container allows for horizontal swiping left and right.
 * @name Swipeable
 * @param {boolean} [RTL = false] - justifies whether the component should include RTL functionality.
 * @param {node} [children] - Utility for opaque data structure.
 * @param {component} [customElementEnd] - Element that can replace right section. ( <Text>example</Text> ).
 * @param {component} [customElementStart] - Element that can replace left section.( <Text>example</Text> ).
 * @param {number} [size] -  size of an icon.
 * @param {number} [activeOpacity] -  active opacity on option press (if not replaced by the component).
 * @param {string} [labelType] - Type label for component text.
 * @param {array} [buttonsStart] - Array config buttons for left side.
 * @param {array} [buttonsEnd] - Array config buttons for right side.
 * @returns {component}
 */

import React from 'react';
import Text from './Text';
import View from './View';
import Icon from './Icon';
import styled, { withTheme } from 'styled-components/native';
import { getValue } from '../defaults';

const ScrollViewSwipeable = styled.ScrollView`
	width: ${getValue('metrics.screenWidth')};
	flex-grow: ${getValue('swipeable.flexGrow')};
`;

const TextLabel = styled(Text)`
	font-size: ${getValue('text.type.swipeableText.fontSize')};
	line-height: ${getValue('text.type.swipeableText.lineHeight')};
	margin-top: ${getValue('text.type.swipeableText.marginTop')};
	color: ${getValue('text.type.swipeableText.color')};
`;

const ViewWrap = styled(View)`
	width: ${getValue('metrics.screenWidth')};
	height: ${getValue('swipeable.height')};
`;

const ViewWrapContent = styled(View)`
	height: ${getValue('swipeable.height')};
	flex: ${getValue('swipeable.flex')};
`;

const TouchableOpacityButton = styled.TouchableOpacity`
	flex: ${getValue('swipeable.flex')};
	justify-content: ${getValue('swipeable.justifyContent')};
	align-items: ${getValue('swipeable.alignItems')};
`;

const SideBtn = ({
	onPress,
	activeOpacity,
	icon,
	iconSize,
	labelType,
	label
}) => {
	return (
		<TouchableOpacityButton
			activeOpacity={activeOpacity}
			onPress={onPress}
		>
			<ViewWrapContent
				justify='center'
				align='center'
			>
				<Icon
					name={icon}
					size={iconSize}
				/>

				<TextLabel
					type={labelType}
				>
					{label}
				</TextLabel>
			</ViewWrapContent>
		</TouchableOpacityButton>
	)
}

export class Swipeable extends React.Component {

	getScrollBtns(buttons = []) {
		const {
			size,
			labelType,
			activeOpacity,
			RTL = false
		} = this.props;

		return (
			<ViewWrap
				RTL={RTL}
				row
			>
				{
					buttons.map((item, index) => {
						return (
							<SideBtn
								key={index}
								onPress={() => this.onOptionPress(item.onPress)}
								icon={item.icon}
								iconSize={size}
								activeOpacity={activeOpacity}
								labelType={labelType}
								label={item.label}
							/>
						)
					})
				}
			</ViewWrap>
		)
	}

	checkIfRTL = () => {
		const { props } = this;
		const themeDir = getValue('themeDir')(props);
		return themeDir === 'rtl';
	}

	shouldComponentScroll = (screensAmount) => {
		const { buttonsStart, buttonsEnd, RTL, customElementStart, customElementEnd } = this.props;
		const defaultResult = screensAmount === 3 || (screensAmount === 2 && (!!buttonsStart || !!customElementStart));
		if (RTL) {
			if (!this.checkIfRTL()) return defaultResult;
			return (screensAmount === 3 || (screensAmount === 2 && (!!buttonsEnd || !!customElementEnd)));
		} else {
			return defaultResult;
		}
	}

	scrollToInitialPosition = (animated = true) => {
		const {
			buttonsEnd,
			buttonsStart,
			customElementStart,
			customElementEnd,
		} = this.props;

		const screens = (buttonsStart || customElementStart) ?
			((buttonsEnd || customElementEnd) ? 3 : 2)
			:
			((buttonsEnd || customElementEnd) ? 2 : 1);

		if (!this.shouldComponentScroll(screens)) {
			this.scroll.scrollTo({ x: 0, y: 0, animated: animated });
		} else {
			this.scroll.scrollTo({ x: (getValue('metrics.screenWidth')(this.props)), y: 0, animated: animated });
		}
	}

	onOptionPress = (customOnPress) => {
		this.scrollToInitialPosition();
		if (!!customOnPress && typeof customOnPress === 'function') customOnPress();
	}

	componentDidMount() {
		setTimeout(() => this.scrollToInitialPosition(false), 50)
	}

	render() {
		const {
			children,
			customElementEnd,
			customElementStart,
			buttonsEnd,
			buttonsStart,
			RTL
		} = this.props;

		return (
			<ScrollViewSwipeable
				horizontal={true}
				showsHorizontalScrollIndicator={false}
				pagingEnabled={true}
				ref={(ref) => this.scroll = ref}
			>

				{/* for non RTL layout */}
				{!RTL && !!customElementStart &&
					<ViewWrap>
						{customElementStart}
					</ViewWrap>
				}
				{!RTL && !customElementStart && !!buttonsStart && this.getScrollBtns(buttonsStart)}

				{/* for  RTL layout */}
				{!!RTL && (
					this.checkIfRTL() ? (
						!!customElementEnd &&
						<ViewWrap>
							{customElementEnd}
						</ViewWrap>
					) : (
							!!customElementStart &&
							<ViewWrap>
								{customElementStart}
							</ViewWrap>
						))
				}

				{!!RTL && (
					this.checkIfRTL() ? (
						!customElementEnd && !!buttonsEnd && this.getScrollBtns(buttonsEnd)
					) : (
							!customElementStart && !!buttonsStart && this.getScrollBtns(buttonsStart)
						)
				)
				}


				{/* Central component */}
				<ViewWrap>
					{children}
				</ViewWrap>


				{/* for non RTL layout */}
				{!RTL && !!customElementEnd &&
					<ViewWrap>
						{customElementEnd}
					</ViewWrap>
				}
				{!RTL && !customElementEnd && !!buttonsEnd && this.getScrollBtns(buttonsEnd)}


				{/* for  RTL layout */}
				{!!RTL && (
					this.checkIfRTL() ? (
						!!customElementStart &&
						<ViewWrap>
							{customElementStart}
						</ViewWrap>
					) : (
							!!customElementEnd &&
							<ViewWrap>
								{customElementEnd}
							</ViewWrap>
						)
				)
				}

				{!!RTL && (
					this.checkIfRTL() ? (
						!customElementStart && !!buttonsStart && this.getScrollBtns(buttonsStart)
					) : (
							!customElementEnd && !!buttonsEnd && this.getScrollBtns(buttonsEnd)
						)
				)
				}

			</ScrollViewSwipeable>
		);
	}
}

export default withTheme(Swipeable);
