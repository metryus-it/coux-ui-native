/**
 * Switch is a component for getting/showing boolean value or to select from one out of two.
 * @name Switch
 * @param {string} [label] - The label is the text shown  of the Switch.
 * @param {function} [onChange] - Action callback, will return new Switch state
 * @param {component} [textComponent] - Part of the item that can replace label, comes from out, example ( <Text>example</Text> ).
 * @param {string} [textType] - Comes from out indicates type ( h1, h2, h3 ...).
 * @param {string} [errorText] - Takes a parameter from to errorText for view message.
 * @param {string} [errorTextType] - Parameter that indicates the type of text.
 * @param {number} [numberOfLines] - Takes a parameter from to label the number of lines ( 3,4,5...).
 * @param {bool} [disabled] - Blocks access and change of the form field ( true - false ).
 * @param {string} [disabledTextType] - Looks at disabled and if true add defualt style disabled for text.
 * @param {number} [textHorizPadding] - Horizontal padding for text.
 * @returns {component}
 */

import React from 'react';
import { Switch as NativeSwitch, Platform } from 'react-native';
import styled, { withTheme } from 'styled-components/native';
import View from './View';
import Text from './Text';
import { getValue } from '../defaults';


const StyledText = styled(Text)`
padding-horizontal: ${props => props.textHorizPadding ? props.textHorizPadding : 7}
`;
const Container = styled(View).attrs(props => ({
  RTL: props.RTL || false,
  row: true,
}))`
padding-vertical: ${props => props.verticalPadding ? props.verticalPadding : 0}
`;

const SwitchStyled = styled(NativeSwitch).attrs(props => ({
  trackColor: { false: '', true: getValue('switch.backgroundColor')(props) }
}))`
`;

const Switch = ({
  disabled,
  numberOfLines,
  disabledTextType,
  textType,
  textComponent,
  errorText,
  errorTextType,
  onChange,
  RTL,
  label,
  verticalPadding = 0,
  ...props
}) => {

  const switchColors = Platform.OS === 'android' ? {
    trackColor: {
      true: getValue('colors.main')(props),
      false: getValue('colors.disabled')(props)
    },
    thumbColor: getValue('colors.white')(props)
  } : {};

  const switchComponent = (
    <SwitchStyled
      disabled={disabled}
      {...switchColors}
      {...props}
      onChange={(onChange && typeof onChange === 'function') ? (res) => {
        onChange(res.nativeEvent.value);
      } : undefined}
    />
  );

  if (label || textComponent) {
    return (
      <View style={{ borderWidth: 0 }}>
        <Container
          verticalPadding={verticalPadding}
        >
          { switchComponent }
          <StyledText
            numberOfLines={numberOfLines}
            RTL={RTL}
            type={!disabled ? textType : disabledTextType}
          >
            {label || textComponent}
          </StyledText>
        </Container>

        {
          !!errorText &&
          <View
            RTL={RTL}
          >
            <Text type={errorTextType}>
              {errorText}
            </Text>
          </View>
        }
      </View>
    );
  } else {
    return switchComponent;
  }
};

export default withTheme(Switch);
