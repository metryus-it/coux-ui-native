# Button
Button component that able to change view, size, to add icons on the right and left side, to deactivate, to show loading status etc

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| label      | string  | Text that appears inside button |
| textType      | string  | Label text type such as ``h1``, ``h2``, ``lg``, ``sm`` etc. See Text component readme for full list |
| type    | string | Determines button color that described at theme config. Could be as gradient or plain color (Possible options: ``Primary``, ``Secondary``, ``Success``, ``Warning``, ``Danger``) |
| size      | string | Determines button's size. Takes such options as ``lg``, ``df``, ``sm``, ``xs``. If not specified will use button default size (equally to ``df`` option) |
| iconStart     | string  | Icon name (see Icon component) that will appear at left button's side (or right side if RTL enabled) |
| iconEnd | string | Icon name (see Icon component) that will appear at right button's side (or left side if RTL enabled) |
| link | boolean | Switch button to transparent and no borders view |
| hollow | boolean | Turn off button's background color and show border instead |
| disabled | boolean | Makes button not clickable and 30% transparent |
| loading | boolean | Shows spinner instead of text |
| RTL | boolean | justifies whether the component should include the RLT functionality. |
| straightColor | boolean | Setting button's label color as defined in theme button's 'type' section ('color'). Otherwise label color is white by default (except of 'hollow' or 'link' button's options) |
| onPress | function | Function that will be called on button pressing |
