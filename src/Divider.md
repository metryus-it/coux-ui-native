# Divider
A component designed to add vertical indentation between elements, as well as display a horizontal line.

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| height | number | Divider height |
| size | string | Predefined width of divider. ``base``, ``m``, ``tiny``, ``xs``, ``small``, ``s``, ``large``, ``l``, ``x-large``, ``xl`` |
| bordered | bool | Add 1px bottom border to divider |
| borderColor | string | Color of border 'colors.main' by default. |
| width | number | Divider width. Makes sense if border enabled only |
| opacity | number | Opacity of divider's border |
| style | number | Divider custom style |
