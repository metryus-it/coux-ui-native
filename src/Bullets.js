/**
 * Bullets indicator for carousels, sliders etc.
 * @name Button
 * @param {object} [style] - Wrapper's style
 * @param {string} [number] - Number of bullets
 * @param {string} [current] - Index of active bullet
 * @returns {component}
 */

import React, { Component } from 'react';
import styled from 'styled-components/native';
import { getValue } from '../defaults';

const StyledViewBullets = styled.View`
	height: ${ props => props.height ? props.height : 10};
	flex-direction: ${ props => props.flexDirection ? props.flexDirection : 'row'};
	justify-content: ${ props => props.justifyContent ? props.justifyContent : 'center'};
	align-items: ${ props => props.alignItems ? props.alignItems : 'center'};
`;

const StyleBullets = styled.View`
	border-radius: ${  props => props.borderRadius || getValue('bullet.radius')(props)};
	border-color: ${ getValue('colors', 'main')};
	border-width: ${   props => props.borderWidth ? props.borderWidth : getValue('bullet.borderWidth')(props)};
	width: ${   props => props.width || getValue('bullet.size')(props)};
	height: ${  props => props.height || getValue('bullet.size')(props)};
  background-color: ${ getValue('colors', 'inActive')};
	margin-right: ${ props => props.isLast ? null : getValue('bullet.spaceBetween')(props)}
`;

const ActiveBullet = styled.View`
	border-radius: ${   props => props.activeBorderRadius || getValue('bullet.radius')(props)};
	border-color: ${ getValue('colors', 'main')};
	/* border-width: ${  props => props.borderWidth || getValue('bullet.borderWidth')(props)};
	width: ${ props => props.activeWidth || getValue('bullet.size')(props)};
	height: ${ props => props.activeHeight || getValue('bullet.size')(props)}; */
	width: ${ props => props.activeWidth || getValue('bullet.sizeActive')(props)};
	height: ${ props => props.activeHeight || getValue('bullet.sizeActive')(props)};
	background-color: ${ getValue('colors', 'main')};
	margin-right: ${ props => props.isLast ? null : getValue('bullet.spaceBetween')(props)}
`;

export default class Bullets extends Component {

  render() {
    const bullets = [];
    let { number = 0, current = 0, style } = this.props;

    for (let i = 0; i < number; i++) {
      const active = (i) == current;
      const isLast = (i - 1) === number;
      if (active) {
        bullets.push(
          <ActiveBullet
            key={i}
            isLast={isLast}
            {...this.props}
          />
        );
      } else {
        bullets.push(
          <StyleBullets
            isLast={isLast}
            key={i}
            {...this.props}
          />
        );
      }

    }

    return (
      <StyledViewBullets style={style}>
        {bullets}
      </StyledViewBullets>
    );
  }
}
