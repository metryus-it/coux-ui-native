/**
 * PickerSelect the form element that emulates a web select component.
 * @name PickerSelect
 * @param {string} [iconStart] - Icon name (see Icon component) that will appear at left side of item (in RTL mode).
 * @param {string} [iconEnd] - Icon name (see Icon component) that will appear at right side of item (in RTL mode).
 * @param {number} [size] - Icon width and height.
 * @param {string} [text] - Main text that will appear at left side of component or right (in RTL mode).
 * @param {string} [textType] - Main text styling (See Text component). Possible options - h1, h2, sm, lg, etc.
 * @param {string} [borderSize] - Bottom border width of item, if not needed use 0. Default border height and color discribed at defaultTheme.
 * @param {boolean} [longText = false] - Make main text column wider in propotion of 2/3 with secondary one. Use this to put long text in primary text column.
 * @param {boolean} [RTL = false] - justifies whether the component should include the RLT functionality.
 * @param {function} [onPress] - Function that will be called on item pressing.
 * @param {string} [hint] - Text below input.
 * @param {string} [label] - Text that will appear above input.
 * @param {boolean} [error = false] - When true highlights hint message and input's border with red color.
 * @param {func} [onChange = false] - Return changed value as a callback
 * @returns {component}
 */

import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import View from '../src/View';
import Text from '../src/Text';
import ListItem from '../src/ListItem';
import { getValue } from '../defaults';
import styled from 'styled-components/native';
import R from 'ramda';
import Icon from './Icon';

const Wrapper = styled(View)(getValue('textInput.wrapper'));
const InputHolder = styled(View)(props => {
  const { noBorder, disabled, error } = props;
  let borderColorKey = 'textInput.borderColor';
  if (noBorder) borderColorKey = 'colors.transparent';
  if (disabled) borderColorKey = 'colors.disabled';
  if (error) borderColorKey = 'textInput.borderColorError';

  return ({
    ...getValue('textInput.inputHolder')(props),
    borderColor: getValue(borderColorKey)(props)
  })
})
const StyledText = styled(Text)(props => {
  if (props.disabled) {
    return {
      ...getValue('textInput.pickerSelectText')(props),
      color: getValue('colors.disabled')(props)
    }
  }
  return getValue('textInput.pickerSelectText');
});
const TopRow = styled(View)(getValue('textInput.topRow'));
const TextRegular = styled(Text)`
  color: ${getValue('textInput.labelColor')}
`;
const Label = styled(Text)`
  color: ${getValue('textInput.labelColor')};
	font-size: ${getValue('textInput.labelFontSize')};
	font-weight: ${getValue('textInput.labelFontWeight')};
`;
const TextLink = styled(Text)`
  color: ${getValue('textInput.linkColor')}
`;

const IconHolder = styled(TouchableOpacity)`
	align-self: stretch;
	justify-content: center;
	padding-horizontal: ${getValue('textInput.paddingHorizontal')};
`;

const Hint = styled(Text)`
  color: ${props => getValue('colors.' + (props.isError ? 'alert' : 'main'))};
  font-size: ${props => getValue('textInput.hintFontSize')};
  margin-top: ${props => !R.isNil(props.marginTop) ? props.marginTop : getValue('textInput.wrapper.marginTop')};
  margin-horizontal: ${ props => !R.isNil(props.marginHorizontal) ? props.marginHorizontal : getValue('textInput.wrapper.marginHorizontal')}
`;

const ViewWrapError = styled(View)`
  margin-top: ${props => !R.isNil(props.marginTop) ? props.marginTop : getValue('textInput.wrapper.marginTop')};
  margin-horizontal: ${ props => !R.isNil(props.marginHorizontal) ? props.marginHorizontal : getValue('textInput.wrapper.marginHorizontal')}
`;

const TextWrapError = styled(Text)`
  color: ${props => getValue('colors.' + (props.isError ? 'alert' : 'main'))};
`;

export default class PickerSelect extends Component {

  componentDidUpdate(prevProps) {
    let nextValue = prevProps.valueDefault || prevProps.value || false;

    if (nextValue !== this.getValue()) {
      this.reciveValue(nextValue)
    }
  }

  convertedSelectData() {
    const { data } = this.props;
    let returnData = [];
    if (data && data.length) {
      returnData = data.map(item => {
        return typeof item === 'object' ? item : {
          label: item,
          value: item
        };
      });
    }
    return returnData;
  }

  getValue() {
    return this.props.valueDefault || this.props.value || false;
  }

	reciveValue = (value) => {
		if (typeof this.props.onChange == 'function') {
			this.props.onChange(value);
		}
	}

  onPress = () => {
    if (typeof this.props.onPress == 'function') {
      this.props.onPress({
        closeAction: this.props.closeAction || false,
        changeAction: this.props.changeAction || false,
        closeOnChange: this.props.closeOnChange || false,
        title: this.props.title || '',
        text: this.props.text || '',
        data: this.convertedSelectData(),
        additionalData: this.props.additionalData || undefined,
        value: this.props.value || '',
        multiply: this.props.multiply || false,
        controls: this.props.controls || false,
        closeOnSubmit: this.props.closeOnSubmit || false,
      });
    } else {
      console.log('Select/Piker - No action on onPress', true);
    }
  }

  getValueText(code) {
    const valueOriginal = this.getValue();
    let resultArr = [];
    let value = valueOriginal;

    if (typeof value !== 'object' && !Array.isArray(value)) {
      value = [value]
    }

    const dataList = this.convertedSelectData();

    if (!dataList || dataList.length === 0) {
      return '';
    }

    dataList.forEach((item) => {
      if (value.indexOf(item.value) != -1) {
        resultArr.push(item.label);
      }
    });
    return resultArr.join(', ');
  }

  renderIcon(iconName, iconSize, action) {
    const iconClickable = action && typeof action === 'function';
    if (iconName) {
      return (
        <IconHolder
          onPress={iconClickable ? action : () => { }}
          activeOpacity={iconClickable ? .7 : 1}
        >
          <Icon
            name={iconName}
            color={getValue('textInput.color')(this.props)}
            size={iconSize || getValue('textInput.wrapper.iconSize')(this.props)}
          />
        </IconHolder>
      );
    } else {
      return null;
    }
  }


  render() {
    const {
      placeholder,
      onPress,
      multiply,
      errorText,
      errorTextType,
      error,
      disabled = false,
      RTL,
      noBorder,
      iconStartSize,
      iconStartAction,
      iconEnd,
      iconStart,
      iconEndAction,
      iconEndSize,
    } = this.props;

    let valueText = this.getValueText();

    if (!valueText) {
      valueText = placeholder;
    }

    let textInputStyle = {
      paddingHorizontal: iconStart ? getValue('textInput.paddingHorizontal')(this.props) : 0,
    }

    let topText;
    const topTextClickable = this.props.topTextAction && typeof this.props.topTextAction === 'function';

    if (this.props.topText) {
      topText = (
        <TouchableOpacity
          onPress={topTextClickable ? this.props.topTextAction : () => { }}
          activeOpacity={topTextClickable ? .7 : 1}
        >
          {topTextClickable ?
            <TextLink RTL={RTL}>{this.props.topText}</TextLink> :
            <TextRegular RTL={RTL}>{this.props.topText}</TextRegular>
          }
        </TouchableOpacity>
      );
    }

    let hint;
    if (!!this.props.hint) {
      hint = (
        <Hint isError={error} RTL={RTL}>{this.props.hint}</Hint>
      );
    }

    let labelText;
    if (this.props.label) {
      labelText = (
        <Label RTL={RTL}>{this.props.label}</Label>
      );
    }

    let viewErrorText;
    if (errorText) {
      viewErrorText = (
        <ViewWrapError
          RTL={RTL}
        >
          <TextWrapError
            isError={error}
            type={errorTextType}
          >
            {errorText}
          </TextWrapError>
        </ViewWrapError>
      )
    }

    return (
      <Wrapper>
        {labelText || topText ? <TopRow row justify='space-between'>
          {labelText}
          {topText}
        </TopRow> : null}
        <TouchableOpacity
          disabled={disabled}
          onPress={this.onPress}
        >
          <InputHolder
            row
            align='center'
            justify='space-between'
            disabled={disabled}
            noBorder={noBorder}
            error={error}
            RTL={RTL}
          >

            {this.renderIcon(iconStart, iconStartSize, iconStartAction)}
            <StyledText
              disabled={disabled}
              style={textInputStyle}
            >
              {valueText}
            </StyledText>
            {this.renderIcon(iconEnd, iconEndSize, iconEndAction)}
          </InputHolder>
        </TouchableOpacity>
        {viewErrorText}
        {hint}
      </Wrapper>
    );
  }
}
