# Icon
Component displays icon image

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| size | number | Size that sets icon's width and height (icon must have square propotions) in pixels |
| source | string | custom image src expects require object of an image |
| name | string | One of icons name discribed at theme config |
| color | string | Custom icon's color for SVG only |
