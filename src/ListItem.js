/**
 * Component purposed to display one list item, with one or two column text and left/icons or custom components. Optionally clickable
 * @name ListItem
 * @param {component} [componentStart] - Custom component displayed on left side of item (in RTL mode)
 * @param {component} [componentEnd] - Custom component displayed on right side of item (in RTL mode)
 * @param {string} [iconStart] - Icon name (see Icon component) that will appear at left side of item (in RTL mode)
 * @param {string} [iconEnd] - Icon name (see Icon component) that will appear at right side of item (in RTL mode)
 * @param {string} [text] - Main text that will appear at left side of component or on center part if textEnd is absent (in RTL mode)
 * @param {string} [textEnd] - Secondary text that will appear at right side of component (in RTL mode). Optional
 * @param {string} [textType] - Main text styling (See Text component). Possible options - h1, h2, sm, lg, etc
 * @param {string} [textEndType] - Secondary text styling (See Text component). Possible options - h1, h2, sm, lg, etc
 * @param {number} [borderSize] - Bottom border width of item, if not needed use 0. Default border height and color discribed at defaultTheme
 * @param {boolean} [longText = false] - Make main text column wider in propotion of 2/3 with secondary one. Use this to put long text in primary text column
 * @param {boolean} [longTextEnd = false] - Make end text column wider in propotion of 2/3 with secondary one.
 * @param {boolean} [centeredText = false] - Makes primary text column align center
 * @param {boolean} [RTL = false] - justifies whether the component should include the RLT functionality.
 * @param {function} [onPress] - Function that will be called on item pressing
 * @returns {component}
 */

import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import R from 'ramda';
import styled from 'styled-components/native';
import { getValue } from '../defaults';
import Icon from './Icon';
import View from './View';
import Text from './Text';


const Wrapper = styled(View)`
justify-content: center;
border-bottom-width: ${props => !R.isNil(props.borderSize) ? props.borderSize : getValue('listItem.borderSize')};
border-bottom-color: ${getValue('listItem.borderColor')};
`;

const Column = styled(View)`
align-items: center;
justify-content: center;
min-height: ${getValue('listItem.height')};
padding-horizontal: ${getValue('listItem.cellPaddingHorizontal')};
padding-vertical: ${ props => !R.isNil(props.verticalPadding) ? props.verticalPadding : getValue('listItem.cellPaddingVertical')(props)};
`;

const ColumnText = styled(Column)`
flex: ${props => props.longText ? 2 : 1};
justify-content: ${props => props.justifyContent};
`;

const ColumnTextEnd = styled(Column)`
flex: ${props => props.longTextEnd ? 2 : 1};
justify-content: flex-end;
`;

export default class ListItem extends Component {
  render() {
    const {
      componentStart,
      componentEnd,
      iconStart,
      iconEnd,
      text,
      textEnd,
      textType,
      textEndType,
      longText,
      longTextEnd,
      borderSize,
      centeredText,
      RTL,
      verticalPadding,
      onPress
    } = this.props;

    const clickable = onPress || typeof onPress === 'function';

    return (
      <TouchableOpacity
        activeOpacity={clickable ? .7 : 1}
        onPress={clickable ? onPress : () => { }}>
        <Wrapper RTL={RTL} row borderSize={borderSize}>
          {(iconStart || componentStart) &&
            <Column
              row
              RTL={RTL}
              verticalPadding={verticalPadding}
            >
              {componentStart ? componentStart : <Icon name={iconStart} />}
            </Column>
          }
          {text &&
            <ColumnText
              row RTL={RTL}
              justifyContent={centeredText ? 'center' : 'flex-start'}
              longText={longText}
              verticalPadding={verticalPadding}
            >
              <Text RTL type={textType || ''}>{text}</Text>
            </ColumnText>
          }
          {textEnd &&
            <ColumnTextEnd
              verticalPadding={verticalPadding}
              row RTL={RTL}
              longTextEnd={longTextEnd}
            >
              <Text RTL type={textEndType || ''}>{textEnd}</Text>
            </ColumnTextEnd>
          }
          {(iconEnd || componentEnd) &&
            <Column
              row RTL={RTL}
              verticalPadding={verticalPadding}
            >{(componentEnd ? componentEnd : <Icon name={iconEnd} />)}
            </Column>
          }
        </Wrapper>
      </TouchableOpacity>
    )
  }
}
