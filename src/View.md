# View
View with display flex, either the normal view or with the RTL functionallity, all the flex alignments may be controlled by props

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| noRTL | boolean | justifies whether the component should include the RLT functionality. |
| row | boolean | Makes flex-direction to be row if true. |
| align | string | usual flex align-items props ( aka flex-start, flex-end ...). |
| justify | string | usual flex justify-content props ( aka flex-start, flex-end ...). |
