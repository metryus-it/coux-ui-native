# Text
Text component with quick access to text formatting

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| align | string | Horizontal align |
| type | string | Text type string. ``h1``, ``h2``, ``h3``, ``h4``, ``h5``, ``h6``, ``xl``, ``lg``, ``sm``, ``xm``, ``xs``, ``typeError``, ``disabledTextType``, ``checkboxText``, ``switchText``, ``swipeableText``. See defaults/theme.js text->type section. |
| numberOfLines | number | Equivalent of React-native Text numberOfLines prop. |
| RTL | boolean | justifies whether the component should include the RLT functionality. |
| onPress | function | Function that will be called on button pressing |
