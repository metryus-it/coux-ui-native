/**
 * Component displays icon image
 * @name Icon
 * @param {number} [size] - Size that sets icon's width and height (icon must have square propotions) in pixels
 * @param {string} [source] - custom image src expects require object of an image
 * @param {string} [name] - One of icons name discribed at theme config
 * @param {string} [color] - Custom icon's color for SVG only
 * @returns {component}
 */

import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { getValue } from '../defaults';
import { icons } from '../defaults/icons';
import styled from 'styled-components/native';

const StyledIcon = styled.Image`
width: ${props => props.size || (props.height && props.width) || getValue('icon', 'size')(props)};
height: ${props => props.size || (props.width && props.height) || getValue('icon', 'size')(props)};
`;

export default class Icon extends Component {

  render() {
    const { size, source, name, color, style, ...rest } = this.props;

    const destination = icons[name];

    if (!!destination && typeof destination === 'function' || typeof destination === 'object') {
      const IconComponent = destination;

      if (style && typeof style === 'object') {
        return <View style={style}>
          <IconComponent size={size} color={color} {...rest} />
        </View>;
      };

      return <IconComponent size={size} color={color} {...rest} />;

    } else {
      const src = source || destination;
      return (
        <View>
          {src ?
            <StyledIcon {...this.props} source={src} />
            : <Text>No icon</Text>}
        </View>
      );
    }
  }
}
