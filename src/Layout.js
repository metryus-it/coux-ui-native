/**
 * Turning to ScrollView if content height bigger then screen height
 */

import React, { PureComponent } from 'react';
import { Animated, ScrollView, TouchableOpacity, Keyboard } from 'react-native';
import styled from 'styled-components/native';
import { IS_IPHONE_X, getValue } from '../defaults';
import View from './View';

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

const StyledScrollView = styled(AnimatedScrollView)`
flex-grow: 1;
background-color: ${getValue('colors.backgroundColor')};
`;
const StyledView = styled(View)`
background-color: ${getValue('colors.backgroundColor')};
`;

const ViewContentContainer = styled.View`
min-height: ${props => props.scrollHeight};
justify-content: flex-start;
padding-top: ${props => props.paddingHeight};
padding-bottom:${props => props.isIphoneX ? 50 : 30};
background-color: ${getValue('colors.backgroundColor')};
`;

class Layout extends PureComponent {

  state = {
		isReady: false,
    scrollHeight: 0,
    scrollEnabled: false
  }

  getMinHeight = ({ nativeEvent }) => {
    const { layout: { height } } = nativeEvent;
    const newHeight = Math.ceil(height);
    this.setState({
      scrollHeight: newHeight,
    })
  }

  enableScroll = () => {
    this.setState({
      scrollEnabled: true
    });
  }
  disableScroll = () => {
    this.setState({
      scrollEnabled: false
    });
  }

  onContentLayout = ({ nativeEvent }) => {
    const { layout: { height } } = nativeEvent;
    const { scrollHeight } = this.state;
    const ceiledHeight = Math.ceil(height);
    if (scrollHeight >= ceiledHeight) {
      this.disableScroll();
    } else {
      this.enableScroll();
    }
  }

	componentDidMount() {
		const { renderDelay, noRenderDelay } = this.props;
		const delayDuration = this.props.hasOwnProperty('noRenderDelay') ? 0 : renderDelay;
		this.inTimeout = setTimeout(() => {
			this.setState({'isReady': true });
		}, delayDuration);
	}

	componentWillUnmount() {
		clearTimeout(this.inTimeout);
	}

  render() {
    const {
      children,
      contentContainerStyle,
      noScroll,
      refreshControl = false,
    } = this.props;
    const { scrollEnabled, scrollHeight } = this.state;
    // const { paddingHeight, animatedY, onScroll } = this.props.collapsible;
		const paddingHeight = 0;

    if (noScroll && !refreshControl) {
      return (
        <StyledView
          style={[{ flex: 1 }, contentContainerStyle]}
          {...this.props}
        >
          <TouchableOpacity
            onPress={() => { Keyboard.dismiss(); }}
            activeOpacity={1}
            style={[{ flex: 1 }, contentContainerStyle]}
          >
            {this.state.isReady ? children : null}
          </TouchableOpacity>
        </StyledView>
      );
    } else {
      return (
        <StyledScrollView
          onLayout={this.getMinHeight}
          scrollEnabled={scrollEnabled || !!refreshControl}
          // _mustAddThis={animatedY}
          scrollIndicatorInsets={{ top: paddingHeight }}
          scrollEventThrottle={2}
          // onScroll={onScroll}
          refreshControl={refreshControl}
          keyboardShouldPersistTaps='handled'
        >
          <ViewContentContainer
            style={contentContainerStyle}
            scrollHeight={scrollHeight}
            paddingHeight={paddingHeight}
            isIphoneX={IS_IPHONE_X}
            onLayout={this.onContentLayout}
          >
            {this.state.isReady ? children : null}
          </ViewContentContainer>
        </StyledScrollView>
      )
    }

  }
}

export default Layout;
