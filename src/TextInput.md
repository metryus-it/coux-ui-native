# TextInput
Component that expands standart TextInput component with extra icons, texts and custom styling. Receive all native TextInput's props and extra props descibed below

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| iconStart | string | Icon name (see Icon component) that will appear at left side of input (in RTL mode) |
| iconStartSize | string | Left icon size |
| iconStartAction | function | Left icon's action |
| iconStart | string | Icon name (see Icon component) that will appear at right side of input (in RTL mode) |
| iconStartSize | string | Right icon size |
| iconStartAction | function | Right icon's action |
| label | string | Label that will appear above input |
| topText | string | Text above input on right side. May be clickable |
| topTextAction | function | Top text action |
| RTL | boolean | justifies whether the component should include the RLT functionality. |
| error | boolean | When true highlights hint message and input's border with red color |
| disabled | boolean | Use textInput as readonly |
| noBorder | boolean | Make transparent border color |
| isUppercase | boolean | Retrun uppercased string |
| noBg | boolean | Make transparent background color |
| hint | string | Text below input |
| onPress | function | If you want to use TextInput component as button, use onPress prop. Other actions such as onTextChange, topTextAction will be ignored. |
| inputStyle | object | Preset (initial set) input custom style. Some styles will be rewriten by props conditions. |
| infoIconAction | function | Add this function and small clickable 'info' icon will appear next to label. |
