/**
 *  Row with display flex and flex-direction row, either the normal row or
 *  with the RTL functionallity, all the alignments may   be controlled by props;
 * @name Row
 * @param {boolean} [RTL = false] - justifies whether the component should include
 * the RLT functionality.
 * @param {string} [align] - usual flex align-items props ( aka flex-start, flex-end ...).
 * @param {string} [justify] - usual flex justify-content props ( aka flex-start, flex-end ...).
 * @param {number} [horizontalPadding] - Horizontal padding value.
 *  Default value comes from theme (theme.metrics.baseHirozontalPadding)
 * @param {boolean} [noFlex = false] - Disable all flex and RTL functionality. You receive simple View with base margins
 * @returns {component}
 */

import React from 'react';
import styled from 'styled-components/native';
import R from 'ramda';
import { getValue } from '../defaults';


const AppRow = styled.View`
  display: flex;
  flex-direction: ${(props) => props.RTL ? getValue('dir', 'row')(props) : 'row'};
  align-items: ${props => props.align ? props.align : 'center'};
  justify-content: ${props => props.justify ? props.justify : 'center'};
  padding-horizontal: ${props => !R.isNil(props.horizontalPadding) ? props.horizontalPadding : getValue('metrics.baseHorizontalPadding')(props)};
`;

const AppRowNoFlex = styled.View`
  padding-horizontal: ${props => !R.isNil(props.horizontalPadding) ? props.horizontalPadding : getValue('metrics.baseHorizontalPadding')(props)};
`;


const Row = (props) => {
  const { children, noFlex } = props;
	if ( noFlex ) {
		return (
			<AppRowNoFlex {...props}>
			{children}
			</AppRowNoFlex>
		);
	} else {
		return (
			<AppRow {...props}>
			{children}
			</AppRow>
		);
	}
};

export default Row;
