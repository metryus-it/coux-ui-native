/**
 * Text component with quick access to text formatting
 * @name Switch
 * @param {string} [align] - Horizontal align
 * @param {string} [type] - Text type string. h1, h2, h3, h4, h5, h6, xl, lg, sm, xm, xs, typeError, disabledTextType, checkboxText, switchText, swipeableText. See defaults/theme.js text->type section.
 * @param {number} [numberOfLines] - Equivalent of React-native Text numberOfLines prop.
 * @param {boolean} [RTL = false] - justifies whether the component should include the RLT functionality.
 * @param {function} [onPress] - Function that will be called on button pressing
 * @returns {component}
 */

import React, { Component } from 'react';
import styled from 'styled-components/native';
import { getValue, parseClassName } from '../defaults';

const TextDefault = styled.Text(getValue(`text.type.default`));

const TextStyled = styled(TextDefault)(props => {
  const { RTL } = props;
  const initialStyles = getValue(`text.type.${props.type}`)(props) || {};
  const alignValueNonRTL = props.align ? { textAlign: props.align } : {};
  const classNameStylesArray = parseClassName(props);
  if (!RTL) return Object.assign({}, initialStyles, alignValueNonRTL, ...classNameStylesArray);
  let alignValue = getValue('dir', 'left')(props);
  if (props.align) alignValue = getValue('dir', props.align)(props);
  const update = {
    textAlign: alignValue
  };
  return Object.assign({}, initialStyles, ...classNameStylesArray, update);

});

export default class SmartText extends Component {
  render() {
    const {
      children,
      numberOfLines,
      onPress,
    } = this.props;
    return (
      <TextStyled
        numberOfLines={numberOfLines}
        onPress={onPress}
        {...this.props}
      >
        {children}
      </TextStyled>
    );
  }
}
