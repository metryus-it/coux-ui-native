// export CouxUiNative from './src';
import * as UI from './src';

import { cauxConfigIcons as configIcons, getValue, adaptiveValue } from './defaults';
export { IS_IPHONE_X } from './defaults';

export const getThemeValue = getValue;
export const adaptive = adaptiveValue;
export const cauxConfigIcons = configIcons;
export const Button = UI.Button;
export const Layout = UI.Layout;
export const Divider = UI.Divider;
export const View = UI.View;
export const Row = UI.Row;
export const Bullets = UI.Bullets;
export const Icon = UI.Icon;
export const Text = UI.Text;
export const ListItem = UI.ListItem;
export const Checkbox = UI.Checkbox;
export const Switch = UI.Switch;
export const Loader = UI.Loader;
export const Swipeable = UI.Swipeable;
export const PickerSelect = UI.PickerSelect;
export const TextInput = UI.TextInput;

export default {
  ...UI
};
