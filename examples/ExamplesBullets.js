import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Markdown from 'react-native-markdown-renderer';
import styled from 'styled-components/native';

import { Bullets, Divider } from '../src';

const MarkdownHolder = styled.View`
width: 100%;
paddingHorizontal: 10px;
`;

const readme = `# Bullets
Bullets indicator for carousels, sliders etc.

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| style | object | Wrapper's style |
| number | string | Number of bullets |
| current | string | Index of active bullet |
`;

class ExamplesBullets extends Component {

	static navigationOptions = ({ navigation }) => {
		return {
			title: 'Bullets Examples',
		}
	};


	render() {

		return (
			<React.Fragment>
				<MarkdownHolder>
					<Markdown>{readme}</Markdown>
				</MarkdownHolder>
				<Divider size="100" />
				<Text style={{ marginTop: 20 }}> Here will be all Bullets Examples</Text>
				<Text style={{ marginTop: 20 }}>General Bullets</Text>
				<View
					style={styles.exampleBullets}
				>
					<Bullets
						number={3}
						current={1}
					/>
				</View>
				<View
					style={styles.exampleBullets}
				>
					<Bullets
						number={5}
						current={1}
						activeWidth={20}
						width={20}
						height={20}
						activeHeight={20}
						activeBorderRadius={50}
						borderRadius={50}
					/>
				</View>
			</React.Fragment>
		)
	}
}

const styles = StyleSheet.create({
	exampleBullets: {
		marginVertical: 40
	}
})

export default ExamplesBullets;
