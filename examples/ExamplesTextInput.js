import React, { Component } from 'react';
import { Text, Switch } from 'react-native';
import Markdown from 'react-native-markdown-renderer';
import styled from 'styled-components/native';
import { getValue } from '../defaults';
import { _switchRTL } from '../defaults/theme';

import { Button, TextInput, View, ListItem, Divider } from '../src';

const ViewFullWidth = styled.View`
	width: ${ getValue('metrics.screenWidth')};
`;

const Padder = styled.View`
  padding: 0 10px;
`;
const BtnWrap = styled.View`
padding: 3px;
`;
const SwitchRow = styled(View)`
height: 50;
justify-content: space-between;
align-items: center;
padding-horizontal: 15px;
`;
const Title = styled.Text`
fontSize: 20;
text-align: center;
`;
const MarkdownHolder = styled.View`
width: 100%;
paddingHorizontal: 10px;
`;

const readme = `# TextInput
Component that expands standart TextInput component with extra icons, texts and custom styling. Receive all native TextInput's props and extra props descibed below

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| iconStart | string | Icon name (see Icon component) that will appear at left side of input (in RTL mode) |
| iconStartSize | string | Left icon size |
| iconStartAction | function | Left icon's action |
| iconStart | string | Icon name (see Icon component) that will appear at right side of input (in RTL mode) |
| iconStartSize | string | Right icon size |
| iconStartAction | function | Right icon's action |
| label | string | Label that will appear above input |
| topText | string | Text above input on right side. May be clickable |
| topTextAction | function | Top text action |
| RTL | boolean | justifies whether the component should include the RLT functionality. |
| error | boolean | When true highlights hint message and input's border with red color |
| disabled | boolean | Use textInput as readonly |
| noBorder | boolean | Make transparent border color |
| noBg | boolean | Make transparent background color |
| hint | string | Text below input |
| onPress | function | If you want to use TextInput component as button, use onPress prop. Other actions such as onTextChange, topTextAction will be ignored. |
| inputStyle | object | Preset (initial set) input custom style. Some styles will be rewriten by props conditions. |
`;

class TextInputExamples extends Component {

  state = {
    RTL: false,
    hidePassword: true,
    input1: 'Text1',
    input2: 'Text2',
    input3: 'Text3',
    input4: 'Text4',
    input5: 'Text5',
    input6: '',
    input7: 'Text7',
    input8: 'Text8',
  }

  _onChange(value, key) {
    this.setState({
      ...this.state,
      [key]: value
    })
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Button Examples',
    }
  };

  switchRTL = () => {
    const newState = !this.state.RTL;
    _switchRTL(newState);
    this.setState({ RTL: newState });
  }

  render() {
    const { hidePassword } = this.state;

    return (
      <ViewFullWidth>
        <Padder>
          <MarkdownHolder>
            <Markdown>{readme}</Markdown>
          </MarkdownHolder>
          <Divider size='20' />
          <Title>TextInput Demos</Title>

          <ListItem
            text={this.state.RTL ? 'RTL' : 'LTR'}
            componentEnd={<Switch value={this.state.RTL} onValueChange={this.switchRTL} />}
          />

          <TextInput
            topText='Regular Text Input'
            value={this.state.input1}
            onChangeText={text => this._onChange(text, 'input1')}
            RTL
          />
          <TextInput
            topText='With left and right icons'
            value={this.state.input2}
            onChangeText={text => this._onChange(text, 'input2')}
            iconStart='settings'
            iconEnd='email'
            RTL
          />
          <TextInput
            topText='Custom icons size'
            value={this.state.input3}
            onChangeText={text => this._onChange(text, 'input3')}
            iconStart='settings'
            iconStartSize={12}
            iconEnd='email'
            iconEndSize={12}
            RTL
          />
          <TextInput
            topText='Clickable top text and icons'
            value={this.state.input3}
            onChangeText={text => this._onChange(text, 'input3')}
            topTextAction={ () => alert('You pressed me! -TopText') }
            iconStart='search'
            iconStartAction={ () => alert('You pressed me! -IconStart') }
            iconEnd='refresh'
            iconEndAction={ () => alert('You pressed me! -IconEnd') }
            RTL
          />
          <TextInput
            topText='Password input demo'
            value={this.state.input4}
            onChangeText={text => this._onChange(text, 'input4')}
            iconEnd={ hidePassword ? 'eye' : 'eyeHide' }
            iconEndAction={ () => this.setState({ hidePassword: !hidePassword }) }
            secureTextEntry={ this.state.hidePassword }
            RTL
          />
          <TextInput
            topText='Placeholder and hint demo'
            value={this.state.input6}
            onChangeText={text => this._onChange(text, 'input6')}
            placeholder='This is placeholder'
            hint='This is hint'
            keyboardType='numeric'
            RTL
          />
          <TextInput
            topText='Error demo'
            value={this.state.input7}
            onChangeText={text => this._onChange(text, 'input7')}
            hint='Error message'
            error={true}
            RTL
          />

          <TextInput
            topText='Using mask'
            value={this.state.input8}
            onChangeText={text => this._onChange(text, 'input8')}
            mask={"+1 ([000]) [000] [00] [00]"}
            RTL
          />

        </Padder>
      </ViewFullWidth>
    )
  }
}

export default TextInputExamples;
