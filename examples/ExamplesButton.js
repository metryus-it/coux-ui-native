import React, { Component } from 'react';
import { Text, Switch } from 'react-native';
import styled from 'styled-components/native';
import { _switchRTL } from '../defaults/theme';
import Markdown from 'react-native-markdown-renderer';

import { Button, View, Divider } from '../src';

const BtnsRow = styled.View`
display: flex;
flex-direction: row;
align-items: center;
justify-content: center;
flex-wrap: wrap;
margin: 5px 0px 15px;
`;
const BtnWrap = styled.View`
padding: 3px;
`;
const SwitchRow = styled(View)`
height: 50;
justify-content: space-between;
align-items: center;
padding-horizontal: 15px;
`;
const Title = styled.Text`
fontSize: 20;
text-align: center;
`;

const MarkdownHolder = styled.View`
width: 100%;
paddingHorizontal: 10px;
`;

const readme = `# Button
Button component that able to change view, size, to add icons on the right and left side, to deactivate, to show loading status etc
| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| label      | string  | Text that appears inside button |
| textType      | string  | Label text type such as _h1_, _h2_, _lg_, _sm_ etc. See Text component readme for full list |
| type    | string | Determines button color that described at theme config. Could be as gradient or plain color (Possible options: _Primary_, _Secondary_, _Success_, _Warning_, _Danger_) |
| size      | string | Determines button's size. Takes such options as _lg_, _df_, _sm_, _xs_. If not specified will use button default size (equally to _df_ option) |
| iconStart     | string  | Icon name (see Icon component) that will appear at left button's side (or right side if RTL enabled) |
| iconEnd | string | Icon name (see Icon component) that will appear at right button's side (or left side if RTL enabled) |
| link | boolean | Switch button to transparent and no borders view |
| hollow | boolean | Turn off button's background color and show border instead |
| disabled | boolean | Makes button not clickable and 30% transparent |
| loading | boolean | Shows spinner instead of text |
| RTL | boolean | justifies whether the component should include the RLT functionality. |
| straightColor | boolean | Setting button's label color as defined in theme button's 'type' section ('color'). Otherwise label color is white by default (except of 'hollow' or 'link' button's options) |
| onPress | function | Function that will be called on button pressing |
`;

class ButtonExamples extends Component {

  state = {
    RTL: false
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Button Examples',
    }
  };

  switchRTL = () => {
    const newState = !this.state.RTL;
    _switchRTL(newState);
    this.setState({ RTL: newState });
  }

  render() {

    return (
      <React.Fragment>
        <MarkdownHolder>
          <Markdown>{readme}</Markdown>
        </MarkdownHolder>
        <Title>Button sizes</Title>
        <BtnsRow>
          <BtnWrap>
            <Button size="lg" label='Large' />
          </BtnWrap>
          <BtnWrap>
            <Button label='Default' />
          </BtnWrap>
          <BtnWrap>
            <Button size='sm' label='Small' />
          </BtnWrap>
          <BtnWrap>
            <Button size='xs' label='Extra small' />
          </BtnWrap>
        </BtnsRow>

        <Title>Button types</Title>
        <BtnsRow>
          <BtnWrap>
            <Button type='primary' label='Primary' />
          </BtnWrap>
          <BtnWrap>
            <Button type='secondary' label='Secondary' />
          </BtnWrap>
          <BtnWrap>
            <Button type='success' label='Success' />
          </BtnWrap>
          <BtnWrap>
            <Button type='warning' label='Warning' />
          </BtnWrap>
          <BtnWrap>
            <Button type='danger' label='Danger' />
          </BtnWrap>
        </BtnsRow>

        <Title>Hollow buttons</Title>
        <BtnsRow>
          <BtnWrap>
            <Button type='primary' label='Primary' hollow />
          </BtnWrap>
          <BtnWrap>
            <Button type='secondary' label='Secondary' hollow />
          </BtnWrap>
          <BtnWrap>
            <Button type='success' label='Success' hollow />
          </BtnWrap>
          <BtnWrap>
            <Button type='warning' label='Warning' hollow />
          </BtnWrap>
          <BtnWrap>
            <Button type='danger' label='Danger' hollow />
          </BtnWrap>
        </BtnsRow>

        <Title>Link buttons</Title>
        <BtnsRow>
          <BtnWrap>
            <Button type='primary' label='Primary' link />
          </BtnWrap>
          <BtnWrap>
            <Button type='secondary' label='Secondary' link />
          </BtnWrap>
          <BtnWrap>
            <Button type='success' label='Success' link />
          </BtnWrap>
          <BtnWrap>
            <Button type='warning' label='Warning' link />
          </BtnWrap>
          <BtnWrap>
            <Button type='danger' label='Danger' link />
          </BtnWrap>
        </BtnsRow>

        <Title>Icons</Title>
        <BtnsRow>
          <BtnWrap>
            <Button size="lg" label='Large' iconStart='close' />
          </BtnWrap>
          <BtnWrap>
            <Button label='Default' iconStart='close' />
          </BtnWrap>
          <BtnWrap>
            <Button size='sm' label='Small' iconStart='close' />
          </BtnWrap>
          <BtnWrap>
            <Button size='xs' label='Extra small' iconStart='close' />
          </BtnWrap>
        </BtnsRow>
        <BtnsRow>
          <BtnWrap>
            <Button type='primary' label='Primary' iconStart='arrowLeft' iconEnd='arrowRight' hollow />
          </BtnWrap>
          <BtnWrap>
            <Button type='secondary' label='Secondary' iconStart='refresh' hollow />
          </BtnWrap>
          <BtnWrap>
            <Button type='success' label='Success' iconEnd='search' />
          </BtnWrap>
          <BtnWrap>
            <Button type='warning' label='Warning' iconStart='settings' />
          </BtnWrap>
          <BtnWrap>
            <Button type='danger' label='Danger' iconStart='eye' link />
          </BtnWrap>
        </BtnsRow>

        <Title>Full width</Title>
        <BtnWrap>
          <Button type='primary' label='Primary' iconStart='refresh' />
        </BtnWrap>
        <BtnWrap>
          <Button type='secondary' label='Secondary' iconEnd='refresh' />
        </BtnWrap>
        <BtnWrap>
          <Button type='success' label='Success' />
        </BtnWrap>
        <BtnWrap>
          <Button type='warning' label='Warning' />
        </BtnWrap>
        <BtnWrap>
          <Button type='danger' label='Danger' />
        </BtnWrap>

        <Title>RTL demo</Title>
        <SwitchRow row>
          <Text>
            {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchRow>
        <BtnsRow>
          <BtnWrap>
            <Button size="lg" label='Large' iconStart='close' RTL />
          </BtnWrap>
          <BtnWrap>
            <Button label='Default' iconStart='close' RTL />
          </BtnWrap>
          <BtnWrap>
            <Button size='sm' label='Small' iconStart='close' RTL />
          </BtnWrap>
        </BtnsRow>

        <Title>Loading demo</Title>
        <BtnsRow>
          <BtnWrap>
            <Button type="warning" size="lg" label='Large' loading />
          </BtnWrap>
          <BtnWrap>
            <Button type="warning" label='Default' iconStart='close' link loading />
          </BtnWrap>
          <BtnWrap>
            <Button type="warning" size='sm' label='Small' iconStart='close' hollow loading />
          </BtnWrap>
        </BtnsRow>

        <Title>Disabled</Title>
        <BtnsRow>
          <BtnWrap>
            <Button type='primary' label='Press me!' onPress={ () => alert('You pressed me!')} />
          </BtnWrap>
          <BtnWrap>
            <Button type='secondary' label='Secondary' disabled />
          </BtnWrap>
          <BtnWrap>
            <Button type='success' label='Success' disabled />
          </BtnWrap>
          <BtnWrap>
            <Button type='warning' label='Warning' disabled />
          </BtnWrap>
          <BtnWrap>
            <Button type='danger' label='Danger' disabled />
          </BtnWrap>
        </BtnsRow>

      </React.Fragment>
    )
  }
}

export default ButtonExamples;
