import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import styled from 'styled-components/native';
import Markdown from 'react-native-markdown-renderer';

import { Divider } from '../src';

const MarkdownHolder = styled.View`
width: 100%;
paddingHorizontal: 10px;
`;

const readme = `# Divider
A component designed to add vertical indentation between elements, as well as display a horizontal line.

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| height | number | Divider height |
| size | string | Predefined width of divider. _base_, _m_, _tiny_, _xs_, _small_, _s_, _large_, _l_, _x-large_, _xl_ |
| bordered | bool | Add 1px bottom border to divider |
| borderColor | string | Color of border 'colors.main' by default. |
| width | number | Divider width. Makes sense if border enabled only |
| opacity | number | Opacity of divider's border |
| style | number | Divider custom style |
`;

class DividerExamples extends Component {

	static navigationOptions = ({ navigation }) => {
		return {
			title: 'Divider Examples',
		}
	};


	render() {

		return (
			<React.Fragment>
				<MarkdownHolder>
					<Markdown>{readme}</Markdown>
				</MarkdownHolder>
				<Text style={{ marginTop: 20 }}> Here will be all Divider Examples</Text>
				<Text style={{ marginTop: 20 }}>General Divider</Text>
				<Divider
					size={'xl'}
					bordered={true}
				/>
				<Divider
					size={'l'}
					width={'80%'}
					bordered={true}
				/>
				<Text style={{ marginTop: 40 }}>Lorem ipsum dolor sit amet, consectetur</Text>
				<Divider
					size={'l'}
					bordered={false}
				/>
				<Text>Lorem ipsum dolor sit amet, consectetur</Text>
			</React.Fragment>
		)
	}
}

const styles = StyleSheet.create({})


export default DividerExamples;
