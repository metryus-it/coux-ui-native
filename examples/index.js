import React, { Component } from 'react';

import { Layout, Button, View, Text } from '../src';

import ExamplesButton from './ExamplesButton';
import ExamplesLayout from './ExamplesLayout';
import ExamplesView from './ExamplesView';
import ExamplesRow from './ExamplesRow';
import ExamplesDivider from './ExamplesDivider';
import ExamplesBullets from './ExamplesBullets';
import ExamplesIcon from './ExamplesIcon';
import ExamplesText from './ExamplesText';
import ExamplesTextInput from './ExamplesTextInput';
import ExamplesCheckbox from './ExamplesCheckbox';
import ExamplesSwitch from './ExamplesSwitch';
import ExamplesListItem from './ExamplesListItem';
import ExamplesLoader from './ExamplesLoader';
import ExamplesSwipeable from './ExamplesSwipeable';
import ExamplesPickerSelect from './ExamplesPickerSelect';
import styled from 'styled-components/native';

const ButtonStyled = styled(Button)`
  width: 120;
  margin: 20px;
`;
const ButtonWrapper = styled(View)`
  width: 120;
  margin: 20px;
`;

class Examples extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Examples',
    }
  };

  uiComponents = [
    { title: 'Button', link: 'Button', id: 'componentButton' },
    { title: 'Divider', link: 'Divider', id: 'componentDivider' },
    { title: 'Bullets', link: 'Bullets', id: 'componentBullets' },
    { title: 'Checkbox', link: 'Checkbox', id: 'componentCheckbox' },
    { title: 'Switch', link: 'Switch', id: 'componentSwitch' },
    { title: 'Swipeable', link: 'Swipeable', id: 'componentSwipeable' },
    { title: 'PickerSelect', link: 'PickerSelect', id: 'componentPickerSelect' },
    { title: 'Loader', link: 'Loader', id: 'componentLoader' },
    { title: 'ListItem', link: 'ListItem', id: 'componentListItem' },
    { title: 'Text', link: 'Text', id: 'componentText' },
    { title: 'TextInput', link: 'TextInput', id: 'componentInput' },
    { title: 'View', link: 'View', id: 'component6' },
    { title: 'Layout', link: 'Layout', id: 'component7' },
    { title: 'Icon', link: 'Icon', id: 'componentIcon' },
    { title: 'Row', link: 'Row', id: 'componentRow' },
  ];

  getDefaultLayout = () => {
    const { navigation: { push } } = this.props;
    return (
      this.uiComponents.map(item => {
        return (
          <ButtonWrapper key={item.id}>
            <Button
              label={item.title}
              onPress={() => { if (item.link) push('Examples', { screen: item.link }) }}
            />
          </ButtonWrapper>

        )
      })
    )
  }

  getContect = () => {
    const { navigation } = this.props;
    const { state } = navigation || {};
    const screen = !!state && !!state.params && !!state.params.screen && state.params.screen;
    let content;
    switch (screen) {
      case 'Button':
        content = <ExamplesButton />;
        break;
      case 'Divider':
        content = <ExamplesDivider />;
        break;
      case 'Bullets':
        content = <ExamplesBullets />;
        break;
      case 'Checkbox':
        content = <ExamplesCheckbox />;
        break;
      case 'Switch':
        content = <ExamplesSwitch />;
        break;
      case 'Swipeable':
        content = <ExamplesSwipeable />;
        break;
      case 'PickerSelect':
        content = <ExamplesPickerSelect />;
        break;
      case 'Loader':
        content = <ExamplesLoader />;
        break;
      case 'ListItem':
        content = <ExamplesListItem />;
        break;
      case 'View':
        content = <ExamplesView />;
        break;
      case 'Text':
        content = <ExamplesText />;
        break;
      case 'TextInput':
        content = <ExamplesTextInput />;
        break;
      case 'Layout':
        content = <ExamplesLayout />;
        break;
      case 'Icon':
        content = <ExamplesIcon />;
        break;
      case 'Row':
        content = <ExamplesRow />;
        break;
      case 'test':
      default:
        content = this.getDefaultLayout();
        break;
    }
    return content;
  }


  render() {

    const contect = this.getContect();
    return (
      <Layout
        contentContainerStyle={{ justifyContent: 'center' }}
      >
        <View align={'center'}>
          {contect}
        </View>
      </Layout>

    )
  }
}

export default Examples;
