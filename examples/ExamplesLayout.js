import React, { Component } from 'react'
import { Text } from 'react-native'
import { View, Button } from '../src';
import Markdown from 'react-native-markdown-renderer';
import styled from 'styled-components/native';

const MarkdownHolder = styled.View`
width: 100%;
paddingHorizontal: 10px;
`;

const Square = styled.View`
  border-width: 1;
  width: 80;
  height: 80;
  margin: 10px;
  justify-content: center;
  align-items: center;
`;

const readme = `# Layout
Turning to ScrollView if content height bigger then screen height
`;

export default class ExamplesView extends Component {

  state = {
    list: [
      {
        text: 'Some',
        id: 'test1'
      },
      {
        text: 'Text',
        id: 'test2'
      },
      {
        text: 'AAA',
        id: 'test3'
      },
    ]
  }

  toPush = [
    {
      text: 'Hello',
      id: 'test2'
    },
    {
      text: 'It is me ',
      id: 'test3'
    },
    {
      text: 'I was wondering',
      id: 'test3'
    },
  ];

  pushItems = () => {
    const { list } = this.state;
    this.setState({ list: [...list, ...this.toPush] });
  }

  render() {
    return (
      <View >
        <MarkdownHolder>
          <Markdown>{readme}</Markdown>
        </MarkdownHolder>
        <Button label={'Add More Content'} onPress={this.pushItems} />

        <View >
          {this.state.list.map(item => {
            return (
              <Square key={item.id}>
                <Text>{item.text}</Text>
              </Square>
            )
          })}
        </View>


      </View>
    )
  }
}
