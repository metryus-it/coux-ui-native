import React, { Component } from 'react';
import {
	Switch,
	Text,
	View
} from '../src';
import { _switchRTL } from '../defaults/theme';
import styled from 'styled-components/native';
import Markdown from 'react-native-markdown-renderer';

const TextDefault = styled(Text)`
	text-align: center;
	margin-bottom: 20;
`;

const ViewWrap = styled(View)`
	margin-top: 20px;
`;

const MarkdownHolder = styled.View`
width: 100%;
paddingHorizontal: 10px;
`;

const readme = `# Switch
Switch is a component for getting/showing boolean value or to select from one out of two.

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| label | label | The label is the text shown  of the Switch. |
| onChange | onChange | Action callback, will return new Switch state |
| textComponent | textComponent | Part of the item that can replace label, comes from out, example ( <Text>example</Text> ). |
| textType | textType | Comes from out indicates type ( _h1_, _h2_, _h3_ ...). |
| errorText | errorText | Takes a parameter from to errorText for view message. |
| errorTextType | errorTextType | Parameter that indicates the type of text. |
| numberOfLines | numberOfLines | Takes a parameter from to label the number of lines ( 3,4,5...). |
| disabled | disabled | Blocks access and change of the form field ( true - false ). |
| disabledTextType | disabledTextType | Looks at disabled and if true add defualt style disabled for text. |
| textHorizPadding | textHorizPadding | Horizontal padding for text. |
`;

class SwitchExamples extends Component {

	state = {
		RTL: false,
		isChecked: false,
	}

	isChecked = () => {
		this.setState({
			isChecked: !this.state.isChecked
		});
	}

	switchRTL = () => {
		const newState = !this.state.RTL;
		_switchRTL(newState);
		this.setState({ RTL: newState });
	}

	static navigationOptions = ({ navigation }) => {
		return {
			title: 'Switch Examples',
		}
	};

	render() {

		return (
			<React.Fragment>
				<MarkdownHolder>
					<Markdown>{readme}</Markdown>
				</MarkdownHolder>
				<TextDefault style={{ marginTop: 20 }}> Here will be all Switch Examples</TextDefault>
				<TextDefault style={{ marginTop: 20 }}>General Switch</TextDefault>
				<View>
					<ViewWrap>
						<Switch
							value={true}
							textType={'switchText'}
						/>
					</ViewWrap>
					<ViewWrap>
						<Switch
							value={false}
							textType={'switchText'}
							label='Val false'
						/>
					</ViewWrap>
					<ViewWrap>
						<Switch
							value={true}
							textType={'switchText'}
							label='Val true'
						/>
					</ViewWrap>
					<ViewWrap>
						<Switch
							value={true}
							onValueChange={this.switchRTL}
							textType={'switchText'}
							disabled
							disabledTextType='disabledTextType'
							label='disabled val true'
						/>
					</ViewWrap>
					<ViewWrap>
						<Switch
							value={false}
							onValueChange={this.switchRTL}
							disabled
							disabledTextType='disabledTextType'
							label='disabled val false'
						/>
					</ViewWrap>
					<ViewWrap>
						<Switch
							value={false}
							onValueChange={this.switchRTL}
							disabled
							disabledTextType='disabledTextType'
							label='disabled val false and error message'
							errorText='Error message'
							errorTextType='typeError'
						/>
					</ViewWrap>
					<ViewWrap>
						<Switch
							value={true}
							onValueChange={this.switchRTL}
							disabled
							disabledTextType='disabledTextType'
							label='disabled val false and error message'
							errorText='Error message'
							errorTextType='typeError'
						/>
					</ViewWrap>
					<ViewWrap>
						<Switch
							value={this.state.RTL}
							RTL
							textType={'switchText'}
							onValueChange={this.switchRTL}
							label='With RTL'
						/>
					</ViewWrap>
				</View>
			</React.Fragment>
		)
	}
}

export default SwitchExamples;
