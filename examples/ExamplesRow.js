import React, { Component } from 'react'
import { Text, Switch } from 'react-native'
import { Row, View } from '../src';
import Markdown from 'react-native-markdown-renderer';
import styled from 'styled-components/native';
import { _switchRTL } from '../defaults/theme';

const MarkdownHolder = styled.View`
  width: 100%;
  paddingHorizontal: 10px;
`;

const Square = styled.View`
  border-width: 1;
  width: 80;
  height: 80;
  margin: 10px;
  justify-content: center;
  align-items: center;
`;

const SwitchRow = styled(Row)`
    height: 50;
    border-color: green;
    border-width: 1;
`;

const StyledText = styled.Text`
margin-top: 30;
`;

const readme = `# Row
Row with display flex and flex-direction row, either the normal row or with the RTL functionallity, all the alignments may   be controlled by props;

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| RTL | bool | justifies whether the component should include the RLT functionality. |
| align | string | usual flex align-items props ( aka flex-start, flex-end ...). |
| justify | string | usual flex justify-content props ( aka flex-start, flex-end ...). |
| horizontalPadding | number | Horizontal padding value. Default value comes from theme (theme.metrics.baseHirozontalPadding) |
| noFlex | bool | Disable all flex and RTL functionality. You receive simple View with base margins |
`;


export default class ExamplesView extends Component {

  state = {
    RTL: false
  }

  testItems = [
    {
      text: 'FIRST',
      id: 'test1'
    },
    {
      text: 'SECOND',
      id: 'test2'
    },
    {
      text: 'THIRD',
      id: 'test3'
    },
  ];

  switchRTL = () => {
    const newState = !this.state.RTL;
    _switchRTL(newState);
    this.setState({ RTL: newState });
  }

  render() {
    return (
      <View column>
        <MarkdownHolder>
          <Markdown>{readme}</Markdown>
        </MarkdownHolder>
        {/* No RTl Row Exampe  */}
        <StyledText> Row No RLT </StyledText>
        <SwitchRow justify={'space-between'} >
          <Text>
            {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchRow>
        <Row >
          {this.testItems.map(item => {
            return (
              <Square key={item.id}>
                <Text>{item.text}</Text>
              </Square>
            )
          })}
        </Row>

        {/* RTl Row Exampe  */}
        <StyledText> Row  RLT </StyledText>
        <SwitchRow RTL justify={'space-between'} >
          <Text>
            {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchRow>
        <Row RTL>
          {this.testItems.map(item => {
            return (
              <Square key={item.id}>
                <Text>{item.text}</Text>
              </Square>
            )
          })}
        </Row>


        {/* RTl Row Exampe  with two Items */}
        <StyledText> Row  RLT ( 2 items aligned Start ) </StyledText>
        <SwitchRow RTL justify={'space-between'} >
          <Text>
            {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchRow>
        <Row RTL justify={'flex-start'}>
          {this.testItems.slice(0, 2).map(item => {
            return (
              <Square key={item.id}>
                <Text>{item.text}</Text>
              </Square>
            )
          })}
        </Row>

        {/* RTl Row Exampe  with two Items */}
        <StyledText> Row  RLT ( 2 items aligned End ) </StyledText>
        <SwitchRow RTL justify={'space-between'} >
          <Text>
            {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchRow>
        <Row RTL justify={'flex-end'}>
          {this.testItems.slice(0, 2).map(item => {
            return (
              <Square key={item.id}>
                <Text>{item.text}</Text>
              </Square>
            )
          })}
        </Row>

        {/* RTl Row Exampe  with two Items */}
        <StyledText> Row  RLT ( 2 items aligned between ) </StyledText>
        <SwitchRow RTL justify={'space-between'} >
          <Text>
            {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchRow>
        <Row RTL justify={'space-between'}>
          {this.testItems.slice(0, 2).map(item => {
            return (
              <Square key={item.id}>
                <Text>{item.text}</Text>
              </Square>
            )
          })}
        </Row>

        {/* RTl Row Exampe  with two Items */}
        <StyledText> Row  RLT ( 2 items aligned around ) </StyledText>
        <SwitchRow RTL justify={'space-between'} >
          <Text>
            {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchRow>
        <Row RTL justify={'space-around'}>
          {this.testItems.slice(0, 2).map(item => {
            return (
              <Square key={item.id}>
                <Text>{item.text}</Text>
              </Square>
            )
          })}
        </Row>


      </View>
    )
  }
}
