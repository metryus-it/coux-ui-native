import React, { Component } from 'react';
import { Switch } from 'react-native';
import {
  Checkbox,
  Text,
  View
} from '../src';
import Markdown from 'react-native-markdown-renderer';
import { _switchRTL } from '../defaults/theme';
import styled from 'styled-components/native';

const SwitchText = styled(View)`
    height: 50;
    justify-content: space-between;
    align-items: center;
    border-color: green;
    border-width: 1;
		margin-bottom: 20;
`;

const TextDefault = styled(Text)`
	text-align: center;
	margin-bottom: 20;
`;

const ViewWrap = styled(View)`
	margin-top: 20px;
`;

const MarkdownHolder = styled.View`
width: 100%;
padding-horizontal: 10;
`;

const readme = `# Checkbox
Checkbox use control parameter with two states - checked and !checked && disabled;

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| label | string | Checkbox label. |
| checked | bool | Shows the state of the checkbox [true, false]. |
| onChange | function | Function to execute on change. |
| textComponent | component | Part of the item that can replace label, example [ <Text>example</Text> ]. |
| textType | string | Comes from out indicates type [_h1_, _h2_, _h3_, _h4_...]. |
| iconActive | string | Takes an active icon name. |
| icon | string | Takes an icon name. |
| iconComponent | component | A component to replace Icon. |
| iconActiveComponent | component | A component to replace Active Icon. |
| size | number | Icon size as per the theme. |
| errorText | string | if there is an error - the text it should display, aka 'required'. |
| errorTextType | string | Parameter that indicates the type of text. |
| numberOfLines | number | Takes a parameter from to label the number of lines [2, 3, 4, 5...]. |
| disabled | bool | The dispabled state [true, false]. |
| RTL | bool | If should enable the RTL functionality [true, false]. |
| checkboxTextDisabled | bool | Looks at disabled and if true add default style disabled for text. |
`;

class CheckboxExamples extends Component {

  state = {
    RTL: false,
    isChecked: false
  }

  isChecked = () => {
    this.setState({
      isChecked: !this.state.isChecked
    });
  }

  switchRTL = () => {
    const newState = !this.state.RTL;
    _switchRTL(newState);
    this.setState({ RTL: newState });
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Checkbox Examples',
    }
  };

  render() {

    return (
      <React.Fragment>
        <MarkdownHolder>
          <Markdown>{readme}</Markdown>
        </MarkdownHolder>
        <TextDefault style={{ marginTop: 20 }}> Here will be all Checkbox Examples</TextDefault>
        <TextDefault style={{ marginTop: 20 }}>General Checkbox</TextDefault>
        <SwitchText RTL row>
          <Text>
            Checkbox {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchText>
        <View>
          <ViewWrap>
            <Checkbox
              label='check-box-1 default without onPress'
              iconActive='checkMark'
              textType={'checkboxText'}
              size={20}
              icon='close'
              numberOfLines={3}
              RTL
            />
          </ViewWrap>
          <ViewWrap>
            <Checkbox
              label='check-box-1 default'
              checked={this.state.isChecked}
              onChange={this.isChecked}
              textType={'checkboxText'}
              iconActive='checkMark'
              size={20}
              icon='close'
              numberOfLines={3}
              RTL
            />
          </ViewWrap>
          <ViewWrap>
            <Checkbox
              label='check-box required message'
              checked={this.state.isChecked}
              onChange={this.isChecked}
              iconActive='checkMark'
              size={20}
              icon='close'
              numberOfLines={3}
              errorTextType='typeError'
              errorText='Text error'
              textType={'checkboxText'}
              RTL
            />
          </ViewWrap>
          <ViewWrap>
            <Checkbox
              label='check-box-3 disabled'
              checked={this.state.isChecked}
              textType={'checkboxText'}
              onChange={this.isChecked}
              iconActive='checkMark'
              size={20}
              icon='close'
              numberOfLines={3}
              disabled
              disabledTextType='disabledTextType'
              RTL
            />
          </ViewWrap>
        </View>
      </React.Fragment>
    )
  }
}

export default CheckboxExamples;
