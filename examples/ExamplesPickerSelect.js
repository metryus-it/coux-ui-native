import React, { Component } from 'react';
import {
	Text,
	View,
	PickerSelect,
	Switch
} from '../src';
import Markdown from 'react-native-markdown-renderer';
import { _switchRTL, getValue } from '../defaults/theme';
import styled from 'styled-components/native';

const TextDefault = styled(Text)`
	text-align: center;
	margin-bottom: 20;
`;

const SwitchText = styled(View)`
    height: 50;
    justify-content: space-between;
    align-items: center;
    border-color: green;
    border-width: 1;
		margin-bottom: 20;
`;

const MarkdownHolder = styled.View`
width: 100%;
paddingHorizontal: 10px;
`;

const readme = `# PickerSelect
PickerSelect the form element that emulates a web select component.

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| iconStart | string | Icon name (see Icon component) that will appear at left side of item (in RTL mode). |
| iconEnd | string | Icon name (see Icon component) that will appear at right side of item (in RTL mode). |
| size | number | Icon width and height. |
| text | string | Main text that will appear at left side of component or right (in RTL mode). |
| textType | string | Main text styling (See Text component). Possible options - _h1_, _h2_, _sm_, _lg_, etc. |
| borderSize | number | Bottom border width of item, if not needed use 0. Default border height and color discribed at defaultTheme. |
| longText | bool | Make main text column wider in propotion of 2/3 with secondary one. Use this to put long text in primary text column. |
| RTL | bool | justifies whether the component should include the RLT functionality. |
| onPress | function | Function that will be called on item pressing. |
| hint | string | Text below input. |
| label | string | Text that will appear above input. |
| error | string | When true highlights hint message and input's border with red color. |
| onChange | function | Return changed value as a callback |
`;

class ExamplesPickerSelect extends Component {

	state = {
		RTL: false,
		isChecked: false
	}

	isChecked = () => {
		this.setState({
			isChecked: !this.state.isChecked
		});
	}

	switchRTL = () => {
		const newState = !this.state.RTL;
		_switchRTL(newState);
		this.setState({ RTL: newState });
	}

	static navigationOptions = ({ navigation }) => {
		return {
			title: 'PickerSelect Examples',
		}
	};

	value = ['2', '3'];
	array = [
		{ label: 'Free merchant', value: '1' },
		{ label: 'Authorized Merchant', value: '2' },
		{ label: 'Limited Company', value: '3' },
		{ label: 'Non Profit Organization', value: '4' },
	];

	render() {

		return (
			<React.Fragment>
				<MarkdownHolder>
					<Markdown>{readme}</Markdown>
				</MarkdownHolder>
				<TextDefault style={{ marginTop: 20 }}> Here will be all PickerSelect Examples</TextDefault>
				<TextDefault style={{ marginTop: 20 }}>General PickerSelect</TextDefault>
				<SwitchText RTL row>
					<Text>
						Checkbox {this.state.RTL ? 'RTL' : 'LTR'}
					</Text>
					<Switch value={this.state.RTL} onValueChange={this.switchRTL} />
				</SwitchText>
				<PickerSelect
					size={20}
					iconEnd='arrowBottom'
					placeholder='SelectPicker RTL'
					borderSize={1}
					RTL
				/>
				<PickerSelect
					size={20}
					data={this.array}
					value={this.value}
					iconEnd='arrowBottom'
					borderSize={1}
					label='Picker with multiline value'
					hint='Text Hint'
					error
					RTL
				/>
				<PickerSelect
					size={20}
					label='Text label'
					iconEnd='arrowBottom'
					placeholder='SelectPicker Label RTL'
					borderSize={1}
					RTL
				/>
				<PickerSelect
					size={20}
					hint='Text Hint'
					error
					iconEnd='arrowBottom'
					placeholder='SelectPicker Label RTL'
					borderSize={1}
					RTL
				/>
				<PickerSelect
					size={20}
					errorText='Text error'
					errorTextType='errorTextType'
					iconEnd='arrowBottom'
					placeholder='SelectPicker Label RTL'
					borderSize={1}
					RTL
				/>
				<PickerSelect
					size={20}
					errorText='Text error'
					errorTextType='errorTextType'
					iconStart='arrowBottom'
					textEnd
					placeholder='SelectPicker Label RTL'
					borderSize={1}
					RTL
				/>

			</React.Fragment>
		)
	}
}

export default ExamplesPickerSelect;
