import React, { Component } from 'react';
import styled from 'styled-components/native';
import { Text } from 'react-native';
import { Icon } from '../src';
import Markdown from 'react-native-markdown-renderer';

const IconsRow = styled.View`
display: flex;
flex-direction: row;
align-items: center;
justify-content: center;
flex-wrap: wrap;
margin: 5px 0px 15px;
`;
const IconWrap = styled.View`
padding: 3px;
`;
const MarkdownHolder = styled.View`
width: 100%;
padding-horizontal: 10px;
`;

const readme = `# Icon
Component displays icon image

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| size | number | Size that sets icon's width and height (icon must have square propotions) in pixels |
| source | string | custom image src expects require object of an image |
| name | string | One of icons name discribed at theme config |
`;

class ExamplesIcon extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Icon Examples',
    }
  };

  render() {
    return (
      <React.Fragment>
        <MarkdownHolder>
          <Markdown>{readme}</Markdown>
        </MarkdownHolder>
        <Text>All default icons:</Text>
        <IconsRow>
          <IconWrap><Icon name='arrowLeft' /></IconWrap>
          <IconWrap><Icon name='arrowRight' /></IconWrap>
          <IconWrap><Icon name='arrowTop' /></IconWrap>
          <IconWrap><Icon name='arrowBottom' /></IconWrap>
          <IconWrap><Icon name='close' /></IconWrap>
          <IconWrap><Icon name='checkMark' /></IconWrap>
          <IconWrap><Icon name='email' /></IconWrap>
          <IconWrap><Icon name='hamburger' /></IconWrap>
          <IconWrap><Icon name='plus' /></IconWrap>
          <IconWrap><Icon name='refresh' /></IconWrap>
          <IconWrap><Icon name='search' /></IconWrap>
          <IconWrap><Icon name='settings' /></IconWrap>
          <IconWrap><Icon name='trashBin' /></IconWrap>
          <IconWrap><Icon name='eye' /></IconWrap>
          <IconWrap><Icon name='eyeHide' /></IconWrap>
          <IconWrap><Icon name='heart' /></IconWrap>
          <IconWrap><Icon name='pin' /></IconWrap>
          <IconWrap><Icon name='user' /></IconWrap>
        </IconsRow>
        <Text>Different sizes:</Text>
        <IconsRow>
          <IconWrap><Icon size={10} name='trashBin' /></IconWrap>
          <IconWrap><Icon size={15} name='trashBin' /></IconWrap>
          <IconWrap><Icon size={20} name='trashBin' /></IconWrap>
          <IconWrap><Icon size={30} name='trashBin' /></IconWrap>
          <IconWrap><Icon size={20} name='trashBin' /></IconWrap>
          <IconWrap><Icon size={15} name='trashBin' /></IconWrap>
          <IconWrap><Icon size={10} name='trashBin' /></IconWrap>
        </IconsRow>
        <Text>Adding with require:</Text>
        <IconsRow>
          <IconWrap><Icon source={ require('../assets/icons/search.png') } /></IconWrap>
          <IconWrap><Icon source={ require('../assets/icons/settings.png') } /></IconWrap>
          <IconWrap><Icon source={ require('../assets/icons/trash-bin.png') } /></IconWrap>
        </IconsRow>
        <Text>Not existing icon:</Text>
        <IconsRow>
          <IconWrap><Icon name='somename' /></IconWrap>
        </IconsRow>
      </React.Fragment>
    )
  }
}

export default ExamplesIcon;
