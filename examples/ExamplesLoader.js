import React, { Component } from 'react';
import { StyleSheet, Switch } from 'react-native';
import styled from 'styled-components/native';
import { Text, View, Loader } from '../src';
import Markdown from 'react-native-markdown-renderer';
import { getValue } from '../defaults';
import { _switchRTL } from '../defaults/theme';

const ViewFullWidth = styled(View)`
	width: ${ getValue('metrics.screenWidth')};
`;

const TextDefault = styled(Text)`
	text-align: center;
`;

const ViewWrap = styled(View)`
	margin-top: 20px;
`;

const MarkdownHolder = styled.View`
width: 100%;
paddingHorizontal: 10px;
`;

const readme = `# Loader
Loader displays a circular loading indicator.
`;

class ExamplesLoader extends Component {

	state = {
		RTL: false,
	}

	switchRTL = () => {
		const newState = !this.state.RTL;
		_switchRTL(newState);
		this.setState({ RTL: newState });
	}

	static navigationOptions = ({ navigation }) => {
		return {
			title: 'Loader Examples',
		}
	};

	render() {
		return (
			<ViewFullWidth>
				<MarkdownHolder>
					<Markdown>{readme}</Markdown>
				</MarkdownHolder>
				<TextDefault style={{ marginTop: 20 }}> Here will be all Loader Examples</TextDefault>
				<TextDefault style={{ marginTop: 20, marginBottom: 20 }}>General Loader</TextDefault>
				<ViewWrap>
					<Loader
						size='small'
						color='black'
					/>
				</ViewWrap>
				<ViewWrap>
					<Loader
						size='large'
						color='red'
					/>
				</ViewWrap>
			</ViewFullWidth>
		)
	}
}

export default ExamplesLoader;
