import React, { Component } from 'react'
import { Text, Switch } from 'react-native'
import { View } from '../src';
import styled from 'styled-components/native';
import { _switchRTL } from '../defaults/theme';
import Markdown from 'react-native-markdown-renderer';


const Square = styled.View`
  border-width: 1;
  width: 80;
  height: 80;
  margin: 10px;
  justify-content: center;
  align-items: center;
`;

const SwitchRow = styled(View)`
    height: 50;
    justify-content: space-between;
    align-items: center;
    border-color: green;
    border-width: 1;
`;
const MarkdownHolder = styled.View`
  width: 100%;
  padding-horizontal: 10px;
	margin-bottom: 30px;
`;

const readme = `# View
View with display flex, either the normal view or with the RTL functionallity, all the flex alignments may be controlled by props

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| noRTL | boolean | justifies whether the component should include the RLT functionality. |
| row | boolean | Makes flex-direction to be row if true. |
| align | string | usual flex align-items props ( aka flex-start, flex-end ...). |
| justify | string | usual flex justify-content props ( aka flex-start, flex-end ...). |
`;

export default class ExamplesView extends Component {

  state = {
    RTL: false
  }

  testItems = [
    {
      text: 'FIRST',
      id: 'test1'
    },
    {
      text: 'SECOND',
      id: 'test2'
    },
    {
      text: 'THIRD',
      id: 'test3'
    },
  ];

  switchRTL = () => {
    const newState = !this.state.RTL;
    _switchRTL(newState);
    this.setState({ RTL: newState });
  }

  render() {
    return (
      <View >
        <MarkdownHolder>
          <Markdown>{readme}</Markdown>
        </MarkdownHolder>
        <Text> Column No RLT </Text>
        <SwitchRow >
          <Text>
            {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchRow>
        <View >
          {this.testItems.map(item => {
            return (
              <Square key={item.id}>
                <Text>{item.text}</Text>
              </Square>
            )
          })}
        </View>

        <Text> Examples of Row   </Text>

        <SwitchRow RTL row>
          <Text>
            ROW {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchRow>

        <View RTL row  >
          {this.testItems.map(item => {
            return (
              <Square key={item.id}>
                <Text>{item.text}</Text>
              </Square>
            )
          })}
        </View>

        <SwitchRow RTL row>
          <Text>
            COLUMN {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchRow>

        <View RTL align={'flex-start'}>
          {this.testItems.map(item => {
            return (
              <Square key={item.id}>
                <Text>{item.text}</Text>
              </Square>
            )
          })}
        </View>

        <SwitchRow RTL >
          <Text>
            COLUMN (Align End By Default) {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchRow>

        <View RTL align={'flex-end'}>
          {this.testItems.map(item => {
            return (
              <Square key={item.id}>
                <Text>{item.text}</Text>
              </Square>
            )
          })}
        </View>

        {/* { For the Justify props } */}

        <SwitchRow RTL >
          <Text>
            COLUMN (Justify Start - 2 items) {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchRow>

        <View style={{ height: 400, borderWidth: 1 }} RTL justify={'flex-start'} align={'flex-end'}>
          {this.testItems.slice(0, 2).map(item => {
            return (
              <Square key={item.id}>
                <Text>{item.text}</Text>
              </Square>
            )
          })}
        </View>

        <SwitchRow RTL >
          <Text>
            COLUMN (Justify Start - 2 items) {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchRow>

        <View RTL style={{ height: 300, borderWidth: 1 }} justify={'flex-end'} align={'flex-end'}>
          {this.testItems.slice(0, 2).map(item => {
            return (
              <Square key={item.id}>
                <Text>{item.text}</Text>
              </Square>
            )
          })}
        </View>

      </View>
    )
  }
}
