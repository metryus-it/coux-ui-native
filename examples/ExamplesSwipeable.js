import React, { Component } from 'react';
import { Switch } from 'react-native';
import Markdown from 'react-native-markdown-renderer';
import {
  Text,
  View,
  Icon,
  Swipeable,
} from '../src';
import { _switchRTL } from '../defaults/theme';
import { getValue } from '../defaults/';
import styled from 'styled-components/native';

const SwitchText = styled(View)`
    height: 50;
    justify-content: space-between;
    align-items: center;
    border-color: green;
    border-width: 1;
		margin-bottom: 20;
`;

const TextDefault = styled(Text)`
	text-align: center;
	margin-bottom: 20;
`;

const TouchableOpacityButton = styled.TouchableOpacity`
	flex: ${getValue('swipeable.flex')};
	justify-content: ${getValue('swipeable.justifyContent')};
	align-items: ${getValue('swipeable.alignItems')};
`;

const ViewWrapContent = styled(View)`
	height: ${getValue('swipeable.height')};
	flex: ${getValue('swipeable.flex')};
`;

const TextLabel = styled(Text)`
	font-size: ${getValue('text.type.swipeableText.fontSize')};
	line-height: ${getValue('text.type.swipeableText.lineHeight')};
	margin-top: ${getValue('text.type.swipeableText.marginTop')};
	color: ${getValue('text.type.swipeableText.color')};
`;

const ViewWrapSwipeable = styled(View)`
	height: 65px;
	display: flex;
	align-items: center;
	justify-content: center;
`;

const ComponentText = styled(Text)`
	color: ${getValue('colors.main')};
	font-size: 20px;
	line-height: 20px;
	text-align: center;
`;

const MarkdownHolder = styled.View`
width: 100%;
paddingHorizontal: 10px;
`;

const readme = `# Swipeable
Swipeable component allows for implementing swipeable rows or similar interaction. It renders its children within a panable container allows for horizontal swiping left and right.

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| RTL | boolean | justifies whether the component should include RTL functionality. |
| children | node | Utility for opaque data structure. |
| customElementEnd | component | Element that can replace right section. ( <Text>example</Text> ). |
| customElementStart | component | Element that can replace left section.( <Text>example</Text> ). |
| size | number | size of an icon. |
| activeOpacity | number | active opacity on option press (if not replaced by the component). |
| labelType | string | Type label for component text. |
| buttonsStart | array | Array config buttons for left side. |
| buttonsEnd | array | Array config buttons for right side. |
`;


const customeElementEnd = <TouchableOpacityButton>
  <View
    row
  >
    <ViewWrapContent
      justify='center'
      align='center'
    >
      <Icon name='eyeHide' />
      <TextLabel>
        Text component 1
					</TextLabel>
    </ViewWrapContent>
    <ViewWrapContent
      justify='center'
      align='center'
    >
      <Icon name='eyeHide' />
      <TextLabel>
        Text component 2
					</TextLabel>
    </ViewWrapContent>
  </View>
</TouchableOpacityButton>;

const customeElementStart = <TouchableOpacityButton>
  <View
    row
  >
    <ViewWrapContent
      justify='center'
      align='center'
    >
      <Icon name='eye' />
      <TextLabel>
        Text component 1
					</TextLabel>
    </ViewWrapContent>
    <ViewWrapContent
      justify='center'
      align='center'
    >
      <Icon name='eye' />
      <TextLabel>
        Text component 2
					</TextLabel>
    </ViewWrapContent>
  </View>
</TouchableOpacityButton>;

const getButtonsStart = () => {
  const buttons = [
    {
      label: 'Delete',
      onPress: () => null,
      icon: 'eyeHide',
      backgroundType: 'btnColorDelete',
    },
    {
      label: 'Contacts',
      onPress: () => null,
      backgroundType: 'mainBtnScrollLight',
      icon: 'eye',
    },
  ];
  return buttons;
}

const getButtonsEnd = () => {
  const buttons = [
    {
      label: 'Delete',
      onPress: () => null,
      icon: 'user',
      backgroundType: 'btnColorDelete'
    },
    {
      label: 'Contacts',
      onPress: () => null,
      backgroundType: 'mainBtnScrollLight',
      icon: 'close'
    },
  ];
  return buttons;
}

class SwipeableExamples extends Component {

  state = {
    RTL: false,
    isChecked: false
  }

  isChecked = () => {
    this.setState({
      isChecked: !this.state.isChecked
    });
  }

  switchRTL = () => {
    const newState = !this.state.RTL;
    _switchRTL(newState);
    this.setState({ RTL: newState });
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Swipeable Examples',
    }
  };

  render() {

    return (
        <React.Fragment>
        <MarkdownHolder>
          <Markdown>{readme}</Markdown>
        </MarkdownHolder>
        <TextDefault style={{ marginTop: 20 }}> Here will be all Swipeable Examples</TextDefault>
        <TextDefault style={{ marginTop: 20 }}>General Swipeable</TextDefault>
        <SwitchText RTL row>
          <Text>
            Swipeable {this.state.RTL ? 'RTL' : 'LTR'}
          </Text>
          <Switch value={this.state.RTL} onValueChange={this.switchRTL} />
        </SwitchText>
        <View>
          <Swipeable
            buttonsStart={getButtonsStart()}
            RTL
          >
            <ViewWrapSwipeable>
              <ComponentText>
                Swipeable start button (RTL)
							</ComponentText>
            </ViewWrapSwipeable>
          </Swipeable>
          <Swipeable
            buttonsEnd={getButtonsEnd()}
          >
            <ViewWrapSwipeable>
              <ComponentText>
                Swipeable end button
							</ComponentText>
            </ViewWrapSwipeable>
          </Swipeable>
          <Swipeable
            buttonsStart={getButtonsStart()}
            buttonsEnd={getButtonsEnd()}
          >
            <ViewWrapSwipeable>
              <ComponentText>
                Both button
							</ComponentText>
            </ViewWrapSwipeable>
          </Swipeable>

          <Swipeable
            customElementStart={customeElementStart}
            customElementEnd={customeElementEnd}
            RTL
          >
            <ViewWrapSwipeable>
              <ComponentText>
                Swipeable Both button component (RTL)
							</ComponentText>
            </ViewWrapSwipeable>
          </Swipeable>

          <Swipeable
            customElementEnd={customeElementEnd}
            RTL
          >
            <ViewWrapSwipeable>
              <ComponentText>
                Swipeable button end component (RTL)
							</ComponentText>
            </ViewWrapSwipeable>
          </Swipeable>

          <Swipeable
            customElementStart={customeElementStart}
            RTL
          >
            <ViewWrapSwipeable>
              <ComponentText>
                Swipeable button start component (RTL)
							</ComponentText>
            </ViewWrapSwipeable>
          </Swipeable>

        </View>
      </React.Fragment>
    )
  }
}

export default SwipeableExamples;
