import React, { Component } from 'react';
import { StyleSheet, Switch } from 'react-native';
import styled from 'styled-components/native';
import { Text, View } from '../src';
import Markdown from 'react-native-markdown-renderer';
import { getValue } from '../defaults';
import { _switchRTL } from '../defaults/theme';

const ViewFullWidth = styled.View`
	width: ${ getValue('metrics.screenWidth')};
`;

const TextDefault = styled.Text`
	text-align: ${'center'};
`;

const SwitchText = styled(View)`
    height: 50;
    justify-content: space-between;
    align-items: center;
    border-color: green;
    border-width: 1;
		margin-bottom: 20;
`;

const MarkdownHolder = styled.View`
width: 100%;
paddingHorizontal: 10px;
`;

const readme = `# Text
Text component with quick access to text formatting

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| align | string | Horizontal align |
| type | string | Text type string. _h1_, _h2_, _h3_, _h4_, _h5_, _h6_, _xl_, _lg_, _sm_, _xm_, _xs_, _typeError_, _disabledTextType_, _checkboxText_, _switchText_, _swipeableText_. See defaults/theme.js text->type section. |
| numberOfLines | number | Equivalent of React-native Text numberOfLines prop. |
| RTL | boolean | justifies whether the component should include the RLT functionality. |
| onPress | function | Function that will be called on button pressing |
`;

class ExamplesText extends Component {

	state = {
		RTL: false,
	}

	switchRTL = () => {
		const newState = !this.state.RTL;
		_switchRTL(newState);
		this.setState({ RTL: newState });
	}

	static navigationOptions = ({ navigation }) => {
		return {
			title: 'Text Examples',
		}
	};

	render() {
		return (
			<ViewFullWidth>
				<MarkdownHolder>
					<Markdown>{readme}</Markdown>
				</MarkdownHolder>
				<TextDefault style={{ marginTop: 20 }}> Here will be all Text Examples</TextDefault>
				<TextDefault style={{ marginTop: 20, marginBottom: 20 }}>General Text</TextDefault>
				<SwitchText RTL row>
					<Text>
						Text {this.state.RTL ? 'RTL' : 'LTR'}
					</Text>
					<Switch value={this.state.RTL} onValueChange={this.switchRTL} />
				</SwitchText>
				<Text RTL>
					Font - default
				</Text>
				<Text RTL type={'h1'}>
					Font - h1
				</Text>
				<Text RTL type={'h2'}>
					Font - h2
				</Text>
				<Text RTL type={'h3'}>
					Font - h3
				</Text>
				<Text RTL type={'h4'}>
					Font - h4
				</Text>
				<Text RTL type={'h5'}>
					Font - h5
				</Text>
				<Text RTL type={'h6'}>
					Font - h6
				</Text>
				<Text RTL type={'xl'}>
					Font - xl
				</Text>
				<Text RTL type={'lg'}>
					Font - lg
				</Text>
				<Text RTL type={'sm'}>
					Font - sm
				</Text>
				<Text RTL type={'xm'}>
					Font - xm
				</Text>
				<Text RTL type={'xs'}>
					Font - xs
				</Text>
			</ViewFullWidth>
		)
	}
}

export default ExamplesText;
