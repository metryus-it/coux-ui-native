import React, { Component } from 'react';
import { StyleSheet, Switch } from 'react-native';
import styled from 'styled-components/native';
import { Text, View, ListItem } from '../src';
import Markdown from 'react-native-markdown-renderer'
import { getValue } from '../defaults';
import { _switchRTL } from '../defaults/theme';

import { Icon, Divider } from '../src';

const Title = styled(Text)`
	paddingHorizontal: 5
`;
const ViewFullWidth = styled.View`
	width: ${ getValue('metrics.screenWidth')};
`;
const SwitchText = styled(View)`
    height: 50;
    justify-content: space-between;
    align-items: center;
    border-color: green;
    border-width: 1;
		margin-bottom: 20;
`;

const MarkdownHolder = styled.View`
width: 100%;
paddingHorizontal: 10px;
`;

const readme = `# ListItem
Component purposed to display one list item, with one or two column text and left/icons or custom components. Optionally clickable

| Props             | Type      | Description |
| ----------------- | --------- | ----------- |
| componentStart | component | Custom component displayed on left side of item (in RTL mode) |
| componentEnd | component | Custom component displayed on right side of item (in RTL mode) |
| iconStart | string | Icon name (see Icon component) that will appear at left side of item (in RTL mode) |
| iconEnd | string | Icon name (see Icon component) that will appear at right side of item (in RTL mode) |
| text | string | Main text that will appear at left side of component or on center part if textEnd is absent (in RTL mode) |
| textEnd | string | Secondary text that will appear at right side of component (in RTL mode). Optional |
| textType | string | Main text styling (See Text component). Possible options - _h1_, _h2_, _sm_, _lg_ etc |
| textEndType | string | Secondary text styling (See Text component). Possible options - _h1_, _h2_, _sm_, _lg_ etc |
| borderSize | number | Bottom border width of item, if not needed use 0. Default border height and color discribed at defaultTheme |
| longText | boolean | Make main text column wider in propotion of 2/3 with secondary one. Use this to put long text in primary text column |
| longTextEnd | boolean | Make end text column wider in propotion of 2/3 with secondary one. |
| centeredText | boolean | Makes primary text column align center |
| RTL | boolean | justifies whether the component should include the RLT functionality. |
| onPress | function | Function that will be called on item pressing |
`;

class ExamplesText extends Component {

	state = {
		RTL: false,
	}

	switchRTL = () => {
		const newState = !this.state.RTL;
		_switchRTL(newState);
		this.setState({ RTL: newState });
	}

	static navigationOptions = ({ navigation }) => {
		return {
			title: 'ListItem Examples',
		}
	};

	render() {
		return (
			<ViewFullWidth>
				<MarkdownHolder>
					<Markdown>{readme}</Markdown>
				</MarkdownHolder>
				<Divider size='25' />
				<Title type='h1' RTL>ListItem Examples</Title>
				<Divider size='25' />

				<Title type='h2' RTL>RTL demo</Title>
				<ListItem
					text={this.state.RTL ? 'RTL' : 'LTR'}
					componentEnd={<Switch value={this.state.RTL} onValueChange={this.switchRTL} />}
				/>
				<Divider size='25' />
				<Title type='h2' RTL>Text only</Title>
				<ListItem
					text='Text left'
					textEnd='Text right'
					RTL
				/>
				<ListItem
					text='Long text long text long text long text long text long text long text long text long text long text'
					textEnd='Short text'
					longText
					RTL
				/>
				<ListItem
					text='Centered text'
					centeredText
					RTL
				/>

				<Divider size='25' />
				<Title type='h2' RTL>Using icons</Title>
				<ListItem
					text='Icons both sides & text in the middle'
					iconStart='user'
					iconEnd='arrowRight'
					centeredText
					RTL
				/>
				<ListItem
					text='Icon left and centered text'
					iconStart='user'
					centeredText
					RTL
				/>
				<ListItem
					iconStart='user'
					textEnd='Icon left and text'
					RTL
				/>
				<ListItem
					text='Icon right and centered text'
					iconEnd='arrowRight'
					centeredText
					RTL
				/>
				<ListItem
					text='Icon right and text'
					iconEnd='arrowRight'
					RTL
				/>
				<ListItem
					text='Text left + icon'
					textEnd='Text right + icon'
					iconStart='user'
					iconEnd='arrowRight'
					RTL
				/>
				<ListItem
					text='Text left'
					textEnd='Text right + icon'
					iconEnd='arrowRight'
					RTL
				/>
				<ListItem
					text='Text left + icon'
					textEnd='Text right'
					iconStart='user'
					RTL
				/>

				<Divider size='25' />
				<Title type='h2' RTL>Using components</Title>
				<ListItem
					text='Components both sides & text in the middle'
					componentStart={<Switch />}
					componentEnd={<Switch />}
					RTL
				/>
				<ListItem
					text='Text and component on right'
					componentEnd={<Switch />}
					RTL
				/>
				<ListItem
					textEnd='Text and component on left'
					componentStart={<Switch />}
					RTL
				/>

				<Divider size='25' />
				<Title type='h2' RTL>Text styling</Title>
				<ListItem
					text='Text left'
					textEnd='Text right'
					textType='xm'
					textType='lg'
					textEndType='xl'
					RTL
				/>
				<ListItem
					text='Long text smaller long text smaller long text smaller long text smaller'
					textType='xm'
					textEnd='Bigger text'
					textEndType='lg'
					longText
					RTL
				/>
				<ListItem
					text='Centered H3 text'
					textType='h3'
					centeredText
					RTL
				/>

				<Divider size='25' />
				<Title type='h2' RTL>Other</Title>
				<ListItem
					iconEnd='arrowRight'
					text='Clickable'
					textEnd='Click me'
					onPress={ () => alert('You clicked me!') }
					RTL
				/>
				<ListItem
					iconEnd='arrowRight'
					text='3px border size'
					borderSize={3}
					RTL
				/>
				<ListItem
					iconEnd='arrowRight'
					text='10px border size'
					borderSize={10}
					RTL
				/>
			</ViewFullWidth>
		)
	}
}

export default ExamplesText;
