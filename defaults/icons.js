const defaultIcons = {
  arrowLeft: require('../assets/icons/arrow-left.png'),
  arrowRight: require('../assets/icons/arrow-right.png'),
  arrowTop: require('../assets/icons/arrow-top.png'),
  arrowBottom: require('../assets/icons/arrow-bottom.png'),
  close: require('../assets/icons/close.png'),
  checkMark: require('../assets/icons/check-mark.png'),
  email: require('../assets/icons/email.png'),
  hamburger: require('../assets/icons/hamburger.png'),
  plus: require('../assets/icons/plus.png'),
  refresh: require('../assets/icons/refresh.png'),
  search: require('../assets/icons/search.png'),
  settings: require('../assets/icons/settings.png'),
  trashBin: require('../assets/icons/trash-bin.png'),
  eye: require('../assets/icons/eye.png'),
  eyeHide: require('../assets/icons/eye-hide.png'),
  heart: require('../assets/icons/heart.png'),
  pin: require('../assets/icons/pin.png'),
  user: require('../assets/icons/user.png'),
}

export let icons = {
  ...defaultIcons
};


export const cauxConfigIcons = (iconsObj = defaultIcons) => {
  icons = { ...defaultIcons, ...iconsObj };
}
