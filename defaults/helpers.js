
import * as R from 'ramda';

import { defaultTheme } from './theme';

export const getValue = (path = 'colors', key = null) => {
  return (props) => {
    const withKey = key ? `${path}.${key}` : path;
    const pathArray = withKey.split('.');
    const retrieveValue = R.path(pathArray);
    return retrieveValue(props.theme) || retrieveValue(defaultTheme);
  }
}

export const adaptiveValue = (Normal, Small, Large, XLarge) => {
  return (props) => {
    if (!Normal && Normal !== 0) return null;

    const width = R.path(['theme', 'metrics', 'screenWidth'], props);
    const height = R.path(['theme', 'metrics', 'screenHeight'], props);

    const normalProportionCof = 1.8;

    const currentProportionCof = height / width;

    if (width <= 320 || (width > 320 && width <= 375 && currentProportionCof < normalProportionCof)) {
      if (Small || Small === 0) return Small;
      else return Normal;

    } else if (width > 320 && width <= 375) {
      return Normal;

    } else if (width > 375 && width <= 414) {
      if (Large || Large === 0) return Large;
      else return Normal;

    } else if (width > 414) {
      if (XLarge || XLarge === 0) return XLarge;
      else return Large || Normal;
    }
  }
}

/**
 * Parses the classNames and returns array of style objects.
 * @param {string} className
 * @param {string} propss
 * @returns {array} - An array of style objects.
 */

export const parseClassName = (props) => {
  const { className = false } = props;
  if (!className) return [{}];
  const splitted = className.trim().split(' ');
  if (!splitted || !splitted.length) return [{}];

  return splitted.map(name => {
    const pathArray = name.split('.');
    const retrieveValue = R.path(pathArray);
    const theme = R.path(['theme', 'classNames'], props) || {};
    return retrieveValue(theme) || {};
  });
}
