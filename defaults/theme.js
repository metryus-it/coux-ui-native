import { Dimensions, Platform } from 'react-native';

const { width, height } = Dimensions.get('window');

const notchDims = [780, 812, 844, 852, 896, 926, 932, 1170, 1080, 1284];

function isIphoneWithNotch() {
  return (
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    (notchDims.includes(width) || notchDims.includes(height))
  );
}

const shared = {
  metrics: {
    baseHorizontalPadding: 20,
    screenWidth: width,
    screenHeight: height,
    swipeableHeight: 65,
    baseMargin: 20,
  }
};

export const IS_IPHONE_X = isIphoneWithNotch();

const dirRTL = {
  row: 'row-reverse',
  start: 'flex-end',
  end: 'flex-start',
  left: 'right',
  right: 'left',
  center: 'center'
};
const dirLTR = {
  row: 'row',
  start: 'flex-start',
  end: 'flex-end',
  left: 'left',
  right: 'right',
  center: 'center'
};

export let defaultTheme = {
  themeDir: 'ltr',
  colors: {
    main: '#263550',
    white: '#ffffff',
    alert: '#FF0032',
    inActive: '#D0D8EA',
    disabled: '#B1BBD2',
    backgroundColor: '#ffffff'
  },
  dir: {
    ...dirLTR
  },
  icon: {
    size: 25
  },
  button: {
    type: {
      primary: {
        color: '#1385e5',
        gradient: {
          colors: ['#2591ed', '#1177cd'],
          start: { x: 0.0, y: 0.5 },
          end: { x: 1, y: 0.5 }
        },
        borderRadius: 4
      },
      secondary: {
        color: '#263550',
        gradient: {
          colors: ['#263550', '#adadad'],
          start: { x: 0.0, y: 0.5 },
          end: { x: 1, y: 0.5 }
        },
        borderRadius: 4
      },
      success: {
        color: '#34c240',
        gradient: {
          colors: ['#43cd4f', '#2fae39'],
          start: { x: 0.0, y: 0.5 },
          end: { x: 1, y: 0.5 }
        },
        borderRadius: 4
      },
      warning: {
        color: '#fa9f47',
        gradient: {
          colors: ['#fbac60', '#f9922e'],
          start: { x: 0.0, y: 0.5 },
          end: { x: 1, y: 0.5 }
        },
        borderRadius: 4
      },
      danger: {
        color: '#d64242',
        gradient: {
          colors: ['#db5757', '#d12d2d'],
          start: { x: 0.0, y: 0.5 },
          end: { x: 1, y: 0.5 }
        },
        borderRadius: 4
      },
    },
    size: {
      lg: {
        fontSize: 21,
        height: 50,
        paddingHorizontal: 20,
        iconSize: 20
      },
      df: {
        fontSize: 18,
        height: 40,
        paddingHorizontal: 15,
        iconSize: 16
      },
      sm: {
        fontSize: 16,
        height: 30,
        paddingHorizontal: 10,
        iconSize: 12
      },
      xs: {
        fontSize: 12,
        height: 20,
        paddingHorizontal: 5,
        iconSize: 8
      },
    }
  },
  textInput: {
    color: '#263550',
    placeholderColor: '#B1BBD2',
    iconSize: 14,
    paddingHorizontal: 7,
    labelColor: '#263550',
    linkColor: '#1385e5',
    hintFontSize: 14,
    wrapper: {
      marginTop: 5,
      marginBottom: 15,
      alignSelf: 'stretch',
      marginHorizontal: 12,
    },
    topRow: {
      padding: 0,
    },
    inputHolder: {
      borderBottomWidth: 1,
      paddingVertical: 5,
      minHeight: 35
    },
    input: {
      flex: 1,
    }
  },
  listItem: {
    height: 40,
    iconSize: 20,
    cellPaddingVertical: 5,
    cellPaddingHorizontal: 5,
    borderColor: '#eee',
    borderSize: 1
  },
  pickerSelect: {
    wrapper: {
      fontSize: 16,
      marginBottom: 10,
      alignSelf: 'stretch',
    },
    text: {
      paddingVertical: 2,
    },
    errorText: {
      marginLeft: 5,
    },
    labelColor: '#263550',
    linkColor: '#1385e5',
    borderColorError: '#F41C52',
    borderWidth: 1,
    borderColor: '#D7DBE5',
  },
  text: {
    type: {
      default: {
        fontSize: 14,
        fontWeight: 400,
        letterSpacing: 1,
        color: '#263550',
      },
      h1: {
        fontSize: 32,
      },
      h2: {
        fontSize: 24
      },
      h3: {
        fontSize: 19
      },
      h4: {
        fontSize: 16
      },
      h5: {
        fontSize: 13
      },
      h6: {
        fontSize: 11
      },
      xl: {
        fontSize: 20
      },
      lg: {
        fontSize: 16,
      },
      sm: {
        fontSize: 14,
      },
      xm: {
        fontSize: 12
      },
      xs: {
        fontSize: 10,
      },
      typeError: {
        color: '#ff0000',
        fontSize: 14,
        fontWeight: 400,
        letterSpacing: 1,
      },
      disabledTextType: {
        color: '#444',
        fontSize: 14,
        fontWeight: 400,
        letterSpacing: 1,
        paddingHorizontal: 7
      },
      checkboxText: {
        paddingHorizontal: 7
      },
      switchText: {
        paddingHorizontal: 7
      },
      swipeableText: {
        fontSize: 10,
        lineHeight: 12,
        marginTop: 5,
        color: '#000',
      },
    },
  },
  bullet: {
    borderWidth: 0,
    size: 6,
    sizeActive: 8,
    radius: 6,
    // spaceBetween: 3,
    spaceBetween: 6,
  },
  swipeable: {
    flexGrow: 0,
    height: 65,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#444',
  },
  switch: {
    backgroundColor: '#008AFF'
  },
  ...shared,
}


export const _switchRTL = (rtl = false) => {
  const newDir = rtl ? dirRTL : dirLTR;
  defaultTheme = {
    ...defaultTheme,
    themeDir: rtl ? 'rtl' : 'ltr',
    dir: {
      ...newDir
    }
  }
}
