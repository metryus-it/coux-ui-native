export { cauxConfigIcons } from './icons';
export { IS_IPHONE_X } from './theme';
export { getValue, adaptiveValue, parseClassName } from './helpers';